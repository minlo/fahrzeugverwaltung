package de.deutscherv.drvit.demo.service;

import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.deutscherv.drvit.demo.assembler.BenutzerAssembler;
import de.deutscherv.drvit.demo.dao.IBenutzerRepository;
import de.deutscherv.drvit.demo.dbo.BenutzerDbo;
import de.deutscherv.drvit.demo.model.Benutzer;

//@WebMvcTest(BenutzerService.class)
@RunWith(SpringJUnit4ClassRunner.class)
//@RunWith(MockitoJUnitRunner.class)
public class BenutzerServiceTest {
   
  
   @InjectMocks
   private BenutzerService _service;
  
  @Mock
  private BenutzerAssembler _assembler;

   @Mock
   private IBenutzerRepository _repository;
   
   @Mock
   private RolleService rolleService;
   
  
 
   @Test
   public void testAlleBenutzerSehen() {
      //2 BenutzerDbo in db einfügen
      //Danach wird die Methode : alleBenutzerSehen() aufrufen
      //testen, dass die Anzahl von den ausgegeben Benutzer 2 ist
      
      List<BenutzerDbo> listebenutzerDbo = new ArrayList<>();
      BenutzerDbo benutzerDbo = createBenutzerDbo("JOJO");
      BenutzerDbo benutzerDbo2 = createBenutzerDbo("JOJO2");
      listebenutzerDbo.add(benutzerDbo);
      listebenutzerDbo.add(benutzerDbo2); 
      
      Benutzer benutzer = new Benutzer();
      //benutzer.setName("tes"); 
      
      Mockito.when(_repository.findAll()).thenReturn(listebenutzerDbo);
      //Mockito.doNothing().when(_assembler.mapDbo(benutzerDbo)); 
      //Mockito.when(_assembler.mapDbo((BenutzerDbo) anything())).thenReturn(benutzer);  
      
      List<Benutzer> result = _service.alleBenutzerSehen();
      
      MatcherAssert.assertThat(result.size(), is(2));
      //verify(_repository.findAll(), times(1));
      //verify(_assembler.mapDbo((BenutzerDbo) anything()), times(listebenutzerDbo.size())); 
      
   }

   private BenutzerDbo createBenutzerDbo(String name) {
     BenutzerDbo benutzerDbo = new BenutzerDbo();
     benutzerDbo.setName(name);
      return benutzerDbo;
   }
   
   @Test
   public void testGetBenutzerById() {
      //ein Benutzer mit iener id in db (BenutzerRespository) speichern
      //testGetBenutzerByI() Methode mit dieser Id aufrufen
      //test das Benutzer mit der Id rauskommt
	   BenutzerDbo benutzerDbo = createBenutzerDbo("JOJO");
	   benutzerDbo.setIdNr(1L);
	   
	   Benutzer benutzer = new Benutzer();
	   benutzer.setIdNr(1L);;
	   benutzer.setName("JOJO");
	   
	   Mockito.when(_repository.findById(1L)).thenReturn(Optional.of(benutzerDbo));
	   Mockito.when(_assembler.mapDbo(benutzerDbo)).thenReturn(benutzer);
	   
	   Benutzer benutzer2 = _service.getBenutzerById(benutzerDbo.getIdNr());
	   
	   MatcherAssert.assertThat(benutzer2.getName(), is(benutzerDbo.getName()));
   }

}