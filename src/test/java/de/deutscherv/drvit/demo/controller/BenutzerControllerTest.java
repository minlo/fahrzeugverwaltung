package de.deutscherv.drvit.demo.controller;

import de.deutscherv.drvit.demo.assembler.BenutzerAssembler;
import de.deutscherv.drvit.demo.controller.BenutzerController;
import de.deutscherv.drvit.demo.security.UserServiceImpl;
import de.deutscherv.drvit.demo.service.BenutzerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import javax.sql.DataSource;

import org.hamcrest.MatcherAssert;

//@WebMvcTest(BenutzerController.class)
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class BenutzerControllerTest {
   
   private final String ALLE_BENUTZER = "/benutzer/all";
   private final String BENUTZER_BY_NAME = "/benutzer/get/";
   private final String Update_BENUTZER = "/benutzer/put/updateBenutzer/";
   private final String ADDBENUTZER = "/benutzer/addBenutzer";
   private final String DELETE_BENUTZER = "/benutzer/delete/";
   
   @Autowired
   MockMvc mockMvc;
   
   //Wir wollen zuerst eine Url eingeben(/rest/benutzer/all)
   // Dansch soll ein Server ResponseCode von 200 zurückgegeben werden
   //Und es soll genau deise Response von 200 getested werden. 
   @Test
   public void shouldReturnAlleBenutzer() throws Exception {
     
      MockHttpServletResponse response = mockMvc.perform(get(ALLE_BENUTZER)
         .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
      MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK.value()));
   }
   
   
   //La même stratégie qu'en haut
   @Test
   public void shloudReturnBenutzerById() throws Exception {
      MockHttpServletResponse response = mockMvc.perform(get(BENUTZER_BY_NAME+1)
         .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
      MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK.value()));
   }
   
   @Test
   public void shouldUpdateBenutzerById() throws Exception{
      MockHttpServletResponse response = mockMvc.perform(put(Update_BENUTZER+1)
         .contentType(MediaType.APPLICATION_JSON)
         .content("{}")).andReturn().getResponse();
      MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK));
   }
   
    //eine Url ("/benutzer/benutzerdaten") und ein leeres Objekt eingeben
   //Erwartet wird 201 als response
   @Test
   public void shouldAddBenutzer() throws Exception{
      
      MockHttpServletResponse response = mockMvc.perform(post(ADDBENUTZER)
         .contentType(MediaType.APPLICATION_JSON)
         .content("{}")).andReturn().getResponse();
      MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.CREATED.value()));
   }
   
   //eine Url ("/benutzer/benutzerdaten/delete") + int eingeben
   //Erwartet wird 200 als response
   @Test
   public void shouldDeleteBenutzerById() throws Exception{
      
      MockHttpServletResponse response = mockMvc.perform(delete(DELETE_BENUTZER+1)
         .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
      MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK.value()));
   }


}