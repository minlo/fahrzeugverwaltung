package de.deutscherv.drvit.demo.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import de.deutscherv.drvit.demo.controller.DemoApiController;
;

@WebMvcTest(DemoApiController.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class DemoapiTest {
	//Was wollen wir testen?
	//Wir wollen zuerst eine url (/demo/status) eingeben
	//Danach erwaten wir ein Server Response von 200
	//Ist 200 als server respons, Test bestanden sondern nicht
	
	@Autowired
	MockMvc mockMvc;
	
	@Test
	public void testDemoApi() throws Exception {
		
		MockHttpServletResponse response = mockMvc.perform(get("/demo/status")
		.contentType(MediaType.APPLICATION_JSON))
		.andReturn().getResponse();
		
		MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK));
	}
	
}
