package de.deutscherv.drvit.demo.controller;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import de.deutscherv.drvit.demo.assembler.BetriebAssembler;
import de.deutscherv.drvit.demo.assembler.UnternehmenAssembler;
import de.deutscherv.drvit.demo.service.BetriebService;

@WebMvcTest(BetriebController.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class BetriebControllerTest {
   
   private final String ALLE_BETRIEBS = "/betrieb/all";
      private final String BETRIEB_BY_ID = "/betrieb/get/";
      private final String Update_BETRIEB = "/betrieb/put/updateBetrieb/";
      private final String ADDBETRIEB = "/betrieb/addBetrieb";
      private final String DELETE_BETRIEB = "/betrieb/delete/";
      
      @Autowired
      MockMvc mockMvc;
      
      
      @MockBean
      BetriebService _betriebService;
      
      @MockBean
      BetriebAssembler _betriebAssembler;
      
      @MockBean
      UnternehmenAssembler _unternehmenAssembler;
      
      //Wir wollen zuerst eine Url eingeben(/rest/benutzer/all)
      // Dansch soll ein Server ResponseCode von 200 zurückgegeben werden
      //Und es soll genau deise Response von 200 getested werden. 
      @Test
      public void shouldReturnAllBetriebs() throws Exception {
        
         MockHttpServletResponse response = mockMvc.perform(get(ALLE_BETRIEBS)
            .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
         MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK.value()));
      }
      
      
      //La même stratégie qu'en haut
      @Test
      public void shouldReturnBetriebById() throws Exception {
         MockHttpServletResponse response = mockMvc.perform(get(BETRIEB_BY_ID+1)
            .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
         MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK.value()));
      }
      
      @Test
      public void shouldUpdateBetriebById() throws Exception{
         MockHttpServletResponse response = mockMvc.perform(put(Update_BETRIEB+1)
            .contentType(MediaType.APPLICATION_JSON)
            .content("{}")).andReturn().getResponse();
         MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK.value()));
      }
      
       //eine Url ("/benutzer/benutzerdaten") und ein leeres Objekt eingeben
      //Erwartet wird 201 als response
      @Test
      public void shouldAddbetrieb() throws Exception{
         
         MockHttpServletResponse response = mockMvc.perform(post(ADDBETRIEB)
            .contentType(MediaType.APPLICATION_JSON)
            .content("{}")).andReturn().getResponse();
         assertThat(response.getStatus(), is(HttpStatus.CREATED.value()));
      }
      
      //eine Url ("/benutzer/benutzerdaten/delete") + int eingeben
      //Erwartet wird 200 als response
      @Test
      public void shouldDeleteBetriebById() throws Exception{
         
         MockHttpServletResponse response = mockMvc.perform(delete(DELETE_BETRIEB+1)
            .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
         MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK.value()));
      }
}