package de.deutscherv.drvit.demo.controller;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import de.deutscherv.drvit.demo.assembler.RechtAssembler;
import de.deutscherv.drvit.demo.service.RechtService;

@WebMvcTest(RechtController.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class RechtControllerTest {
   
   
   
      private final String ALLE_RECHT = "/recht/all";
      private final String RECHT_BY_ID = "/recht/get/";
      private final String Update_RECHT = "/recht/put/updateRecht/";
      private final String ADDRECHT = "/recht/addRecht";
      private final String DELETE_RECHT = "/recht/delete/";
      
      @Autowired
      MockMvc mockMvc;
      
      
      @MockBean
      RechtAssembler _rechtAssembler;
      
      @MockBean
      RechtService _reService;
      
      
      
      //Wir wollen zuerst eine Url eingeben(/rest/benutzer/all)
      // Dansch soll ein Server ResponseCode von 200 zurückgegeben werden
      //Und es soll genau deise Response von 200 getested werden. 
      @Test
      public void shouldReturnAlleRecht() throws Exception {
        
         MockHttpServletResponse response = mockMvc.perform(get(ALLE_RECHT)
            .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
         MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK.value()));
      }
      
      
      //La même stratégie qu'en haut
      @Test
      public void shouldReturnRechtById() throws Exception {
         MockHttpServletResponse response = mockMvc.perform(get(RECHT_BY_ID+1)
            .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
         MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK));
      }
      
      @Test
      public void shouldUpdateRechtById() throws Exception{
         MockHttpServletResponse response = mockMvc.perform(put(Update_RECHT+1)
            .contentType(MediaType.APPLICATION_JSON)
            .content("{}")).andReturn().getResponse();
         MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK));
      }
      
       //eine Url ("/benutzer/benutzerdaten") und ein leeres Objekt eingeben
      //Erwartet wird 201 als response
      @Test
      public void shouldAddRecht() throws Exception{
         
         MockHttpServletResponse response = mockMvc.perform(post(ADDRECHT)
            .contentType(MediaType.APPLICATION_JSON)
            .content("{}")).andReturn().getResponse();
         MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.CREATED.value()));
      }
      
      //eine Url ("/benutzer/benutzerdaten/delete") + int eingeben
      //Erwartet wird 200 als response
      @Test
      public void shouldDelteRechtById() throws Exception{
         
         MockHttpServletResponse response = mockMvc.perform(delete(DELETE_RECHT+1)
            .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
         MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK.value()));
      }
}