package de.deutscherv.drvit.demo.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import de.deutscherv.drvit.demo.assembler.RolleAssembler;
import de.deutscherv.drvit.demo.service.RolleService;

@WebMvcTest(RolleController.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class RolleControllerTest {
   
   
   
   private final String ALLE_ROLLE = "/rolle/all";
      private final String ROLLE_BY_ID = "/rolle/get/";
      private final String Update_ROLLE = "/rolle/put/updateRolle/";
      private final String ADDROLLE = "/rolle/addRolle";
      private final String DELETE_ROLLE = "/rolle/delete/";
      
      @Autowired
      MockMvc mockMvc;
      
      
      @MockBean
      RolleAssembler _rolleAssembler;
      
      @MockBean
      RolleService _rolService;
      
      
      
      //Wir wollen zuerst eine Url eingeben(/rest/benutzer/all)
      // Dansch soll ein Server ResponseCode von 200 zurückgegeben werden
      //Und es soll genau deise Response von 200 getested werden. 
      @Test
      public void shouldReturnAlleRolle() throws Exception {
        
         MockHttpServletResponse response = mockMvc.perform(get(ALLE_ROLLE)
            .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
         MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK.value()));
      }
      
      
      //La même stratégie qu'en haut
      @Test
      public void shouldReturnRolleById() throws Exception {
         MockHttpServletResponse response = mockMvc.perform(get(ROLLE_BY_ID+1)
            .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
         MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK));
      }
      
      @Test
      public void shouldUpdateRolleById() throws Exception{
         MockHttpServletResponse response = mockMvc.perform(put(Update_ROLLE+1)
            .contentType(MediaType.APPLICATION_JSON)
            .content("{}")).andReturn().getResponse();
         MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK));
      }
      
       //eine Url ("/benutzer/benutzerdaten") und ein leeres Objekt eingeben
      //Erwartet wird 201 als response
      @Test
      public void shouldAddRolle() throws Exception{
         
         MockHttpServletResponse response = mockMvc.perform(post(ADDROLLE)
            .contentType(MediaType.APPLICATION_JSON)
            .content("{}")).andReturn().getResponse();
         MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.CREATED.value()));
      }
      
      //eine Url ("/benutzer/benutzerdaten/delete") + int eingeben
      //Erwartet wird 200 als response
      @Test
      public void shouldDeleteRolle() throws Exception{
         
         MockHttpServletResponse response = mockMvc.perform(delete(DELETE_ROLLE+1)
            .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
         MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK.value()));
      }
}