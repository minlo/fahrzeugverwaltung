package de.deutscherv.drvit.demo.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import de.deutscherv.drvit.demo.assembler.BetriebAssembler;
import de.deutscherv.drvit.demo.assembler.FahrzeugAssembler;
import de.deutscherv.drvit.demo.service.FahrzeugService;

@WebMvcTest(FahrzeugController.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class FahrzeugControllerTest {
   
   
   
   private final String ALLE_FAHRZEUGE = "/fahrzeug/all";
      private final String FAHRZEUG_BY_ID = "/fahrzeug/get/";
      private final String Update_FAHRZEUG = "/fahrzeug/put/updateFahrzeug/";
      private final String ADDFAHRZEUG = "/fahrzeug/addFahrzeug";
      private final String DELETE_FAHRZEUG = "/fahrzeug/delete/";
      private final String GET_STATUS_FAHRZEUG =  "fahrzeug/get/statusFahrzeugVonBis/";
      private final String GET_FREIE_FAHRZEUG =   "fahrzeug/get/freiFahrzeugVonBis";
      
      @Autowired
      MockMvc mockMvc;
      
      @MockBean
      FahrzeugAssembler _fahrzeugAssembler;
      
      @MockBean
      FahrzeugService _fahrzeugService;
      
      @MockBean
      BetriebAssembler betriebAssembler;
      
      
      
      //Wir wollen zuerst eine Url eingeben(/rest/benutzer/all)
      // Dansch soll ein Server ResponseCode von 200 zurückgegeben werden
      //Und es soll genau deise Response von 200 getested werden. 
      @Test
      public void shouldReturnAlleFahrzeuge() throws Exception {
        
         MockHttpServletResponse response = mockMvc.perform(get(ALLE_FAHRZEUGE)
            .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
         MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK.value()));
      }
      
      
      //La même stratégie qu'en haut
      @Test
      public void shouldReturnFahrzeugById() throws Exception {
         MockHttpServletResponse response = mockMvc.perform(get(FAHRZEUG_BY_ID+1)
            .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
         MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK));
      }
      
      @Test
      public void shouldUpdateFahrzeugById() throws Exception{
         MockHttpServletResponse response = mockMvc.perform(put(Update_FAHRZEUG+1)
            .contentType(MediaType.APPLICATION_JSON)
            .content("{}")).andReturn().getResponse();
         MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK));
      }
      
       //eine Url ("/benutzer/benutzerdaten") und ein leeres Objekt eingeben
      //Erwartet wird 201 als response
      @Test
      public void shouldAddFahrzeug() throws Exception{
         
         MockHttpServletResponse response = mockMvc.perform(post(ADDFAHRZEUG)
            .contentType(MediaType.APPLICATION_JSON)
            .content("{}")).andReturn().getResponse();
         MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.CREATED.value()));
      }
      
      //eine Url ("/benutzer/benutzerdaten/delete") + int eingeben
      //Erwartet wird 200 als response
      @Test
      public void shouldDeleteFahrzeugById() throws Exception{
         
         MockHttpServletResponse response = mockMvc.perform(delete(DELETE_FAHRZEUG+1)
            .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
         MatcherAssert.assertThat(response.getStatus(), is(HttpStatus.OK.value()));
      }
}