package de.deutscherv.drvit.demo.service;

import de.deutscherv.drvit.demo.assembler.RechtAssembler;
import de.deutscherv.drvit.demo.assembler.RolleAssembler;
import de.deutscherv.drvit.demo.dao.IRechtRepository;
import de.deutscherv.drvit.demo.dao.IRolleRepository;
import de.deutscherv.drvit.demo.dbo.RechtDbo;
import de.deutscherv.drvit.demo.dbo.RolleDbo;
import de.deutscherv.drvit.demo.exception.NotFoundRolleException;
import de.deutscherv.drvit.demo.model.Rolle;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
public class RolleService {

   @Autowired
   private IRolleRepository _rolleRepository;

   @Autowired
   private IRechtRepository _rechteRepository;
   
   @Autowired
   private RechtAssembler rechtAssembler;

   @Autowired
   private RolleAssembler _rolleAssembler;

   @Autowired
   private RechtService _rechtService;

   /**
    * liefert alle Rolle.
    * @return liste von Rollen
    */
   public List<Rolle> alleRolle() {
      return _rolleRepository.findAll().stream()
               .map(_rolleAssembler::mapDbo).collect(Collectors.toList());
   }

   /**
    * liefert eine Rolle.
    * @param id Rolle
    * @return rolle by Id
    */
   public Rolle getRolle(@NonNull final Long id) {
      final Optional<RolleDbo> optRolle = _rolleRepository.findById(id);
      if (optRolle.isPresent()) {
         return _rolleAssembler.mapDbo(optRolle.get());
      }
      throw new NotFoundRolleException("Rolle mit id " + id + " nicht vorhanden");
   }

   /**
    * liefert eine RolleDbo.
    * @param id RolleDbo
    * @return rolleDbo by Id
    */
   public RolleDbo getRolleDbo(@NonNull final Long id) {
      final Optional<RolleDbo> optRolle = _rolleRepository.findById(id);
      if (optRolle.isPresent()) {
         return optRolle.get();
      }
      throw new NotFoundRolleException("Rolle mit id " + id + " nicht vorhanden");
   }


   /**
    * modifiziert eine Rolle.
    * @param rolleId  Rolle
    * @param rolle Rolle
    * @return new RolleDbo
    */
   public Rolle updateRolle(@NonNull final Long rolleId, final Rolle rolle) {

      final RolleDbo rolleDbo = getRolleDbo(rolleId);
      rolleDbo.setName(rolle.getName());
      return _rolleAssembler.mapDbo(_rolleRepository.saveAndFlush(rolleDbo));

   }

   /**
    * Speichert eine rolle.
    * @param rolle Rolle
    * @return new RolleDbo
    */
   public Rolle addRolle(@NonNull final Rolle rolle) {

      RolleDbo newRolleDbo = _rolleAssembler.mapDto(rolle);
      newRolleDbo = _rolleRepository.save(newRolleDbo);
      return _rolleAssembler.mapDbo(newRolleDbo);

   }

   /**
    * loescht eine Rolle by Id.
    * @param rolleId Rolle
    */
   public void deleteRolleById(final Long rolleId) {
      final Optional<RolleDbo> optRolle = _rolleRepository.findById(rolleId);
      if (optRolle.isPresent()) {
         _rolleRepository.deleteById(rolleId);
      } else {
         throw new NotFoundRolleException("Rolle mit der id " + rolleId + " existiert nicht");
      }
   }


   /**
    * Speichert ein Recht zu Rolle.
    * @param rechtId Recht
    * @param rolleId Rolle
    * @return new Recht Zu Rolle
    */
   public Rolle addRechtToRole(final Long rechtId, final Long rolleId) {
      final RechtDbo rechtDbo = _rechtService.getRechtDbo(rechtId);
      final RolleDbo rolleDbo = getRolleDbo(rolleId);

      rolleDbo.getRechts().add(rechtDbo);
      rechtDbo.getRolle().add(rolleDbo);

      _rechteRepository.save(rechtDbo);
      
      return _rolleAssembler.mapDbo(_rolleRepository.save(rolleDbo));
   }
}