package de.deutscherv.drvit.demo.service;

import de.deutscherv.drvit.demo.assembler.BetriebAssembler;
import de.deutscherv.drvit.demo.assembler.UnternehmenAssembler;
import de.deutscherv.drvit.demo.dao.IBetriebRepository;
import de.deutscherv.drvit.demo.dao.IUnternehmenRepository;
import de.deutscherv.drvit.demo.dbo.BetriebDbo;
import de.deutscherv.drvit.demo.dbo.UnternehmenDbo;
import de.deutscherv.drvit.demo.exception.NotFoundBetriebException;
import de.deutscherv.drvit.demo.exception.NotFoundUnternehmenException;
import de.deutscherv.drvit.demo.model.Betrieb;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BetriebService {
   @Autowired
   private IBetriebRepository _betriebRepository;

   @Autowired
   private BetriebAssembler _betriebAssembler;

   @Autowired
   private IUnternehmenRepository _unternehmenRepository;
   
   @Autowired
   private UnternehmenService _unternehmenService;
   
   @Autowired
   private UnternehmenAssembler _unternehmenAssembler;


   /**
    * Liefert alle Betriebs zu eienem Unternehmen zurück.
    * @param unternehmenId Unternehmen
    * @return Liste Betriebs zu einem Unternehmen
    */
   public List<Betrieb> alleBetriebEinesUnternehmen(final Long unternehmenId) {
      return _betriebRepository.findBy_UnternehmenId(unternehmenId).stream().map(_betriebAssembler::mapDbo)
               .collect(Collectors.toList());
   }

   /**
    * Liefert den Betrieb mit der uebergebenen Id zurueck.
    * @param id Betrieb
    * @return betrieb
    */
   public Betrieb getBetriebById(@NonNull final Long id) {
      final Optional<BetriebDbo> betriebDbo = _betriebRepository.findById(id);
      if (betriebDbo.isPresent()) {
         return _betriebAssembler.mapDbo(betriebDbo.get());
      } else {
         throw new NotFoundBetriebException("Betrieb mit der Id " + id + " existiert nicht");
      }
   }

   /**
    * Liefert den BetriebDbo mit der uebergebenen Id zurueck.
    * @param id BetreibDbo
    * @return betreibDbo
    */
   public BetriebDbo getBetriebDbo(@NonNull final Long id) {
      final Optional<BetriebDbo> optBetrieb = _betriebRepository.findById(id);
      if (optBetrieb.isPresent()) {
         return optBetrieb.get();
      } else {
         throw new NotFoundBetriebException("Betrieb mit der Id " + id + " existiert nicht");
      }
   }


    /**
     * modifiziert einen Betrieb anhang einer Id.
     * @param betriebId Betrieb
     * @param betrieb Betrieb
     * @return der betrieb Betrieb
     */
   public Betrieb updateBetrieb(@NonNull final Long betriebId, final Betrieb betrieb) {

      final BetriebDbo betriebDbo = getBetriebDbo(betriebId);
      betriebDbo.setName(betrieb.getName());
      return  _betriebAssembler.mapDbo(_betriebRepository.saveAndFlush(betriebDbo));

   }

   /**
    * Speichert einen Betrieb.
    * @param betrieb Betrieb
    * @param unternehmenId Unternehmen
    * @return new betrieb Betreib
    */
   @Transactional(readOnly = false)
   public Betrieb addBetrieb(@NonNull final Betrieb betrieb, final Long unternehmenId) {
      //final Optional<UnternehmenDbo> unternehmenDboOptional = _unternehmenRepository.findById(unternehmenId);
	   UnternehmenDbo unternehmenDbo = _unternehmenService.getUnternehmenDbo(unternehmenId);
	   BetriebDbo betriebDbo = _betriebAssembler.mapDto(betrieb);
	   betriebDbo.setUnternehmen(unternehmenDbo);
	   betriebDbo = _betriebRepository.saveAndFlush(betriebDbo);
	   
	   unternehmenDbo.getBetrieb().add(betriebDbo);
	   _unternehmenRepository.saveAndFlush(unternehmenDbo);
	   
	   return _betriebAssembler.mapDbo(_betriebRepository.saveAndFlush(betriebDbo));
   }

   /**
    * loescht den Betrieb zu einem Unternehmen mit der uebergebenen Id zurueck.
    * @param betriebId Betrieb
    */
   @Transactional(readOnly = false)
   public void deleteBetriebById(final Long betriebId) {
      final Optional<BetriebDbo> optbetrieb = _betriebRepository.findById(betriebId);
      if (optbetrieb.isPresent()) {
         _betriebRepository.deleteById(betriebId);
      } else {
         throw new NotFoundBetriebException("Betrieb mit der id " + betriebId + " existiert nicht");
      }
   }
}