package de.deutscherv.drvit.demo.service;

import de.deutscherv.drvit.demo.assembler.UnternehmenAssembler;
import de.deutscherv.drvit.demo.dao.IUnternehmenRepository;
import de.deutscherv.drvit.demo.dbo.UnternehmenDbo;
import de.deutscherv.drvit.demo.exception.NotFoundBetriebException;
import de.deutscherv.drvit.demo.exception.NotFoundUnternehmenException;
import de.deutscherv.drvit.demo.model.Unternehmen;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
public class UnternehmenService {

   @Autowired
   private IUnternehmenRepository _repository;

   @Autowired
   private UnternehmenAssembler _assembler;

   /**
    * liefert alle Unternehmen zurueck.
    * @return liste Unternehmen
    */
   public List<Unternehmen> alleUnternehmen() {
      final List<UnternehmenDbo> unternehmenDbos = _repository.findAll();
      return unternehmenDbos.stream().map(_assembler::mapDbo).collect(Collectors.toList());

   }

   /**
    * liefert ein Unternehmen zurueck.
    * @param id Unternehmen
    * @return unternehmen
    */
   public Unternehmen getUnternehmen(@NonNull final Long id) {
      final Optional<UnternehmenDbo> optUnternehmen = _repository.findById(id);
      if (optUnternehmen.isPresent()) {
         return _assembler.mapDbo(optUnternehmen.get());
      } else {
         throw new NotFoundUnternehmenException("Unternehmen mit der Id " + id + " existiert nicht");
      }
   }

   /**
    * liefert ein UnternehmenDbo zurueck.
    * @param id UnternehmenDbo
    * @return unternehmenDbo
    */
   public UnternehmenDbo getUnternehmenDbo(@NonNull final Long id) {
      final Optional<UnternehmenDbo> optBetrieb = _repository.findById(id);
      if (optBetrieb.isPresent()) {
         return optBetrieb.get();
      } else {
         throw new NotFoundUnternehmenException("Unternehmen mit der Id " + id + " existiert nicht");
      }
   }
   /**
    * modifiziert einen Unternehmen.
    * @param unternehmenId Unternehmen
    * @param unternehmen Unternehmen
    * @return new Unternehmen
    */
   public Unternehmen updateUnternehmen(@NonNull final Long unternehmenId, final Unternehmen unternehmen) {

      UnternehmenDbo unternehmenZuUpdate = getUnternehmenDbo(unternehmenId);
      unternehmenZuUpdate.setName(unternehmen.getName());
      unternehmenZuUpdate = _repository.saveAndFlush(unternehmenZuUpdate);
      return _assembler.mapDbo(unternehmenZuUpdate);
   }


   /**
    * Speichert ein Unternehmen.
    * @param unternehmen unternehmen
    * @return new Unternehmen
    */
   public Unternehmen addUnternehmen(@NonNull final Unternehmen unternehmen) {
      UnternehmenDbo newUnternehmenDbo = _assembler.mapDto(unternehmen);
      newUnternehmenDbo = _repository.saveAndFlush(newUnternehmenDbo);
      return _assembler.mapDbo(newUnternehmenDbo);
   }


   /**
    * loescht ein Unternehmene.
    * @param unternehmenId Unternehmen
    */
   public void deleteUnternehmenById(@NonNull final Long unternehmenId) {
      final Optional<UnternehmenDbo> optUnternehmen = _repository.findById(unternehmenId);
      if (optUnternehmen.isPresent()) {
         _repository.deleteById(unternehmenId);
      } else {
         throw new NotFoundUnternehmenException(" Id existiert nicht. ");
      }
   }
}