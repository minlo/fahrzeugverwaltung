package de.deutscherv.drvit.demo.service;

import de.deutscherv.drvit.demo.assembler.BenutzerAssembler;
import de.deutscherv.drvit.demo.dao.IBenutzerRepository;
import de.deutscherv.drvit.demo.dbo.BenutzerDbo;
import de.deutscherv.drvit.demo.dbo.RolleDbo;
import de.deutscherv.drvit.demo.dbo.UnternehmenDbo;
import de.deutscherv.drvit.demo.exception.NotFoundBenutzerException;
import de.deutscherv.drvit.demo.model.Benutzer;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BenutzerService {

   /** Benutzer repository.*/
   @Autowired
    private IBenutzerRepository _repository;

   @Autowired
    private BenutzerAssembler _assembler;

   @Autowired
    private RolleService _rolleService;

    /**
     * Liefert alle Benutzer aus.
     * @return Liste aller Benutzer
     */
   @Transactional(readOnly = true)
    public List<Benutzer> alleBenutzerSehen() {
      final List<BenutzerDbo> alleBenutzerDbo = _repository.findAll();
      final List<Benutzer> listeBenutzers =  new ArrayList<>();
      for (BenutzerDbo benutzerDbo : alleBenutzerDbo) {
         final Benutzer mapDbo = _assembler.mapDbo(benutzerDbo);
         listeBenutzers.add(mapDbo);
      }
      return listeBenutzers;
   }

    /**
     * Liefert den Benutzer mit der uebergebenen Id zurueck.
     * @param id Benutzer-Id
     * @return der Benutzer
     */
   public Benutzer getBenutzerById(@NonNull final Long id) {
      final Optional<BenutzerDbo> optBenutzerDbo = _repository.findById(id);
      if (optBenutzerDbo.isPresent()) {
         return _assembler.mapDbo(optBenutzerDbo.get());
      }
      throw new NotFoundBenutzerException("Benutzer existiert nicht " + id);
   }


   /**
    * Liefert den BenutzerDbo mit der uebergebenen Id zurueck.
    * @param id BenutzerDbo
    * @return der BenutzerDbo
    */
   public BenutzerDbo getBenutzerDboById(@NonNull final Long id) {
      final Optional<BenutzerDbo> optBenutzerDbo = _repository.findById(id);
      if (optBenutzerDbo.isPresent()) {
         return optBenutzerDbo.get();
      }
      throw new NotFoundBenutzerException("Benutzer existiert nicht " + id);
   }


    /**
     * modifiziert den Benutzer mit der uebergebenen Id zurueck.
     * @param benutzerId Benutzer
     * @param benutzer Benutzer
     * @return der Benutzer
     */
   public Benutzer updateBenutzer(@NonNull final Long benutzerId, final Benutzer benutzer) {
	  
	  BenutzerDbo benutzerDbo = getBenutzerDboById(benutzerId);
	  benutzerDbo.setName(benutzer.getName());
	  _repository.saveAndFlush(benutzerDbo);
	  return _assembler.mapDbo(benutzerDbo);
   }

    /**
     * Speichert den Benutzer.
     * @param benutzer der Benutzer
     * @return der Benutzer
     */
   public Benutzer addBenutzer(@NonNull final Benutzer benutzer) {
      BenutzerDbo newbenutzerDbo = _assembler.mapDto(benutzer);
      newbenutzerDbo = _repository.saveAndFlush(newbenutzerDbo);
      return _assembler.mapDbo(newbenutzerDbo);
   }


    /**
     * loescht den Benutzer.
     * @param benutzerId Benutzer
     */
   public void deleteBenutzer(@NonNull final  Long benutzerId) {

      final Optional<BenutzerDbo> benutzerDboId = _repository.findById(benutzerId);

      if (benutzerDboId.isPresent()) {
         _repository.deleteById(benutzerId);
      } else {
         throw new NotFoundBenutzerException(" Id existiert nicht. ");
      }
   }


    /**
     * Speichert eine Rolle füer einen Benutzer.
     * @param benutzerId Benutzer
     * @param rolleId Rolle
     * @return neue Rolle
     */
   public Benutzer addRolleToBenutzer(final Long rolleId, final Long benutzerId) {
      final RolleDbo rolleDbo = _rolleService.getRolleDbo(rolleId);
      final BenutzerDbo benutzerDbo = _repository.findById(benutzerId).orElseThrow(EntityNotFoundException::new);
      benutzerDbo.getRolle().add(rolleDbo);
      return _assembler.mapDbo(_repository.save(benutzerDbo));
   }

    /**
     * liefert einen Benutzer per Name.
     * @param username String
     * @return name benutzerDbo
     */
   public Optional<BenutzerDbo> getBenutzerByName(final String username) {
      return _repository.findBy_name(username);
   }
}