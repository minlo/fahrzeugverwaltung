package de.deutscherv.drvit.demo.service;

import de.deutscherv.drvit.demo.assembler.RechtAssembler;
import de.deutscherv.drvit.demo.assembler.RolleAssembler;
import de.deutscherv.drvit.demo.dao.IRechtRepository;
import de.deutscherv.drvit.demo.dbo.RechtDbo;
import de.deutscherv.drvit.demo.exception.NotFoundRechtException;
import de.deutscherv.drvit.demo.model.Recht;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RechtService {

   @Autowired
   private IRechtRepository _rechtRepository;

   @Autowired
   private RechtAssembler _rechtAssembler;
   
   @Autowired
   private RolleAssembler _rolleAssembler;
   
   @Autowired
   private RolleService _rolleService;

   /**
    * liefert alle Rchte zu Rolle zuruck.
    * @param rolleid Rolle
    * @return liste Rechte von Rolle
    */
   public List<Recht> alleRechtEinerRolle(final Long rolleid) {
      return _rechtRepository.findAll().stream()
              .map(_rechtAssembler::mapDbo).collect(Collectors.toList());
   }

   /**
    * liefert Rechte by Id zurueck.
    * @param id Recht
    * @return Recht
    */
   @Transactional(readOnly = true)
   public Recht getRechtById(@NonNull final Long id) {
      final Optional<RechtDbo> optRecht = _rechtRepository.findById(id);
      if (optRecht.isPresent()) {
         return _rechtAssembler.mapDbo(optRecht.get());
      }
      throw new NotFoundRechtException("Recht mit der id " + id + " existiert nicht");
   }

   /**
    * liefert RechtDbo by Id zurueck.
    * @param id RechtDbo
    * @return RechtDbo
    */
   public RechtDbo getRechtDbo(@NonNull final Long id) {
      final Optional<RechtDbo> optRecht = _rechtRepository.findById(id);
      if (optRecht.isPresent()) {
         return optRecht.get();
      }
      throw new NotFoundRechtException("Recht mit der id " + id + " existiert nicht");
   }


   /**
    * modifiziert ein Recht.
    * @param rechtId Recht
    * @param recht Recht
    * @return new Recht
    */
   @Transactional(readOnly = false)
   public Recht updateRecht(@NonNull final Long rechtId, final Recht recht) {
	   
	   RechtDbo rechtDbo = getRechtDbo(rechtId);
	   rechtDbo.setName(recht.getName());
	   rechtDbo = _rechtRepository.saveAndFlush(rechtDbo);
	   
	   return _rechtAssembler.mapDbo(rechtDbo);
   }

   /**
    * speichert  ein Recht.
    * @param recht Recht
    * @return new Recht
    */
   @Transactional(readOnly = false)
   public Recht addRecht(@NonNull final Recht recht) {
      RechtDbo newRecht = _rechtAssembler.mapDto(recht);
      newRecht = _rechtRepository.save(newRecht);
      return _rechtAssembler.mapDbo(newRecht);
   }


   /**
    * loescht ein Recht.
    * @param rechtId Recht
    */
   public void deleteRechtById(@NonNull final Long rechtId) {
      final Optional<RechtDbo> optRecht = _rechtRepository.findById(rechtId);
      if (optRecht.isPresent()) {
         _rechtRepository.deleteById(rechtId);
      } else {
         throw new NotFoundRechtException("Recht mit der id " + rechtId + " existiert nicht");
      }
   }
}