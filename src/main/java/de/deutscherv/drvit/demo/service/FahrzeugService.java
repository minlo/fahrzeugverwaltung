package de.deutscherv.drvit.demo.service;

import de.deutscherv.drvit.demo.assembler.FahrzeugAssembler;
import de.deutscherv.drvit.demo.assembler.ReservierungAssembler;
import de.deutscherv.drvit.demo.dao.IFahrzeugRepository;
import de.deutscherv.drvit.demo.dao.IReservierungRepository;
import de.deutscherv.drvit.demo.dbo.BetriebDbo;
import de.deutscherv.drvit.demo.dbo.FahrzeugDbo;
import de.deutscherv.drvit.demo.dbo.ReservierungDbo;
import de.deutscherv.drvit.demo.exception.NotFoundFahrzeugException;
import de.deutscherv.drvit.demo.model.Fahrzeug;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
public class FahrzeugService {

   @Autowired
   private IFahrzeugRepository _fahrzeugRepository;

   @Autowired
   private IReservierungRepository _reservierungRepository;
   
   @Autowired
   private ReservierungAssembler _reservierungAssembler;

   @Autowired
   private FahrzeugAssembler _fahrzeugAssembler;

   @Autowired
   private BetriebService _betriebService;

   /**
    * liefert alle Fahrzeuge aus.
    * @return liste von Fahrzeugen
    */
   public List<Fahrzeug> alleFahrzeug() {
      return _fahrzeugRepository.findAll().stream()
              .map(_fahrzeugAssembler::mapDbo).collect(Collectors.toList());
   }

   /**
    * liefert ein Fahrzeug mit Id zurueck.
    * @param id Fahrzeug
    * @return fahrzeugder
    */
   public Fahrzeug getFahrzeugById(@NonNull final Long id) {
      final Optional<FahrzeugDbo> optFahrzeug = _fahrzeugRepository.findById(id);
      if (optFahrzeug.isPresent()) {
         return _fahrzeugAssembler.mapDbo(optFahrzeug.get());
      }
      throw new NotFoundFahrzeugException("Fharzeug mit der id " + id + " existiert nicht");
   }

   /**
    * liefert ein FahrzeugDbo mit Id zurueck.
    * @param id FahrzeugDbo
    * @return fahrzeugderDbo
    */
   public FahrzeugDbo getFahrzeugDboById(@NonNull final Long id) {
      final Optional<FahrzeugDbo> optFahrzeug = _fahrzeugRepository.findById(id);
      if (optFahrzeug.isPresent()) {
         return optFahrzeug.get();
      }
      throw new NotFoundFahrzeugException("Fharzeug mit der id " + id + " existiert nicht");
   }

   /**
    * modifiziert einen Fahrzeug anhang einer Id.
    * @param id Fahrzeug
    * @param fahrzeug Fahrzeug
    * @return new fahrzeug
    */
   public Fahrzeug updateFahrzeugById(final Long id, final Fahrzeug fahrzeug) {
      FahrzeugDbo fahrzeugZuUpZuDate = getFahrzeugDboById(id);
      fahrzeugZuUpZuDate.setModel(fahrzeug.getModel());
      fahrzeugZuUpZuDate = _fahrzeugRepository.saveAndFlush(fahrzeugZuUpZuDate);
      return _fahrzeugAssembler.mapDbo(fahrzeugZuUpZuDate);
   }

   /**
    * speichert  ein Fahrzeug.
    * @param betriebId Betrieb
    * @param model Fahrzeug
    * @return new fahrzeug
    */
   public Fahrzeug addFahrzeug(final Long betriebId, final Fahrzeug fahrzeug) {
      final BetriebDbo betriebDbo = _betriebService.getBetriebDbo(betriebId);
      FahrzeugDbo newFahrzeugDbo = _fahrzeugAssembler.mapDto(fahrzeug);
      newFahrzeugDbo.setBetrieb(betriebDbo);
      newFahrzeugDbo = _fahrzeugRepository.saveAndFlush(newFahrzeugDbo);
      return _fahrzeugAssembler.mapDbo(newFahrzeugDbo);
   }

   /**
    * loescht ein Fahrzeug .
    * @param fahrzeugId Fahrzeug
    */
   public void deleteFahrzeugById(final Long fahrzeugId) {
      final Optional<FahrzeugDbo> optFahrzeug = _fahrzeugRepository.findById(fahrzeugId);
      if (optFahrzeug.isPresent()) {
         _fahrzeugRepository.deleteById(fahrzeugId);
      } else {
         throw new NotFoundFahrzeugException("Fahrzeug mit id " + fahrzeugId + " existiert nicht");
      }
   }

   /**
    * prueft ob, ein Fahrzeug reserviert ist oder nicht.
    * @param fahrzeugId Fahrzeug
    * @param date1 Date
    * @param date2 Date
    * @return true oder false
    */
   public Boolean isFahrzeugFreiInZeitraum(@NonNull final Long fahrzeugId, @NonNull final Date date1, @NonNull final Date date2) {
      final List<ReservierungDbo> reservierungDbos = _reservierungRepository
         .findByFahrzeugAndStartReservierungAfterAndEndReservierungBefore(fahrzeugId, date1, date2);
      return reservierungDbos.isEmpty();
   }
   /**
    * liefert Liste von freien Fahrzeugen.
    * @param date1 Date
    * @param date2 Date
    * @return liste von Fahrzeuge
    */
   public List<Fahrzeug> getFreieFahrzeuge(@NonNull final Date date1, @NonNull final Date date2) {
      final List<FahrzeugDbo> fahrzeugList = _fahrzeugRepository.findAll().stream()
         .filter(fahrzeugDbo -> isFahrzeugFreiInZeitraum(fahrzeugDbo.getIdNr(),
         date1, date2)).collect(Collectors.toList());
      return fahrzeugList.stream().map(fahrzeug -> _fahrzeugAssembler.mapDbo(fahrzeug)).collect(Collectors.toList());
   }
}