package de.deutscherv.drvit.demo.service;

import de.deutscherv.drvit.demo.assembler.ReservierungAssembler;
import de.deutscherv.drvit.demo.dao.IReservierungRepository;
import de.deutscherv.drvit.demo.dbo.BenutzerDbo;
import de.deutscherv.drvit.demo.dbo.FahrzeugDbo;
import de.deutscherv.drvit.demo.dbo.ReservierungDbo;
import de.deutscherv.drvit.demo.exception.FahrzeugNotFreiException;
import de.deutscherv.drvit.demo.exception.NotFoundReservierungException;
import de.deutscherv.drvit.demo.model.Reservierung;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
public class ReservierungService {

   @Autowired
   private IReservierungRepository _reservierungRepository;

   @Autowired
   private ReservierungAssembler _reservierungAssembler;

   @Autowired
   private FahrzeugService _fahrzeugService;

   @Autowired
   private BenutzerService _benutzerService;

   /**
    * liefert alle Reservierungen.
    * @return liste Reservierungen von Rolle
    */
   public List<Reservierung> alleReservierung() {
      return _reservierungRepository.findAll().stream()
              .map(_reservierungAssembler::mapDbo).collect(Collectors.toList());
   }

   /**
    * liefert eine bestimmte Reservierung zurueck.
    * @param id Reservierung
    * @return reservierung Reservierung
    */
   public Reservierung getReservierung(@NonNull final Long id) {
      final Optional<ReservierungDbo> optReservierung = _reservierungRepository.findById(id);
      if (optReservierung.isPresent()) {
         return _reservierungAssembler.mapDbo(optReservierung.get());
      }
      throw new NotFoundReservierungException("Reservierung mit der id " + id + " existiert nicht");
   }

   /**
    * modifiziert eine Reservierung.
    * @param reservierungId Reservierung
    * @param reservierung Reservierung
    * @return new Reservierung
    */
   public Reservierung updateReservierung(@NonNull final Long reservierungId, final Reservierung reservierung) {

      final Reservierung reservierungZuUpdate = getReservierung(reservierungId);

      reservierungZuUpdate.setBenutzer(reservierung.getBenutzer());
      reservierungZuUpdate.setStartReservierung(reservierung.getStartReservierung());
      reservierungZuUpdate.setFahrzeug(reservierung.getFahrzeug());
      reservierungZuUpdate.setEndReservierung(reservierung.getEndReservierung());

      ReservierungDbo updatedReservierungDbo = _reservierungAssembler.mapDto(reservierungZuUpdate);
      updatedReservierungDbo = _reservierungRepository.saveAndFlush(updatedReservierungDbo);

      return _reservierungAssembler.mapDbo(updatedReservierungDbo);
   }

   /**
    * Speichert eine Reservierung.
    * @param reservierung Reservierung
    * @param fahrZeugId Fahrzeug
    * @param benutzerId Benutzer
    * @return new reservierungDbo
    */
   public Reservierung addReservierung(@NonNull final Reservierung reservierung, final Long fahrZeugId, final Long benutzerId) {

      final BenutzerDbo benutzerDbo = _benutzerService.getBenutzerDboById(benutzerId);

      final FahrzeugDbo fahrzeugZuReservieren = _fahrzeugService.getFahrzeugDboById(fahrZeugId);

      final Boolean fahrzeugFrei = _fahrzeugService.isFahrzeugFreiInZeitraum(fahrZeugId,
          reservierung.getStartReservierung(),
          reservierung.getEndReservierung());

      if (fahrzeugFrei.equals(Boolean.FALSE)) {
         throw new FahrzeugNotFreiException("Fahzeug ist nicht Frei");
      }

      ReservierungDbo reservierungDbo = _reservierungAssembler.mapDto(reservierung);
      

      reservierungDbo.setFahrzeug(fahrzeugZuReservieren);
      reservierungDbo.setBenutzer(benutzerDbo);
      reservierungDbo.setStartReservierung(reservierung.getStartReservierung());
      reservierungDbo.setEndReservierung(reservierung.getEndReservierung());

      reservierungDbo = _reservierungRepository.saveAndFlush(reservierungDbo);
      benutzerDbo.getReservierung().add(reservierungDbo);
      
      return _reservierungAssembler.mapDbo(reservierungDbo);
   }

   /**
    * loescht eine Reservierung.
    * @param reservierungId ReservierungDbo
    */
   public void deleteReservierungById(@NonNull final Long reservierungId) {
      final Optional<ReservierungDbo> optReservierungDbo = _reservierungRepository.findById(reservierungId);
      if (optReservierungDbo.isPresent()) {
         _reservierungRepository.deleteById(reservierungId);
      } else {
         throw new NotFoundReservierungException("Reservierung mit id " + reservierungId + " existiert nicht");
      }
   }
}