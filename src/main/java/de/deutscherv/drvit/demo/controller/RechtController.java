package de.deutscherv.drvit.demo.controller;

import de.deutscherv.drvit.demo.api.IRechtApi;
import de.deutscherv.drvit.demo.model.Recht;
import de.deutscherv.drvit.demo.service.RechtService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/** Controller für Recht.*/
@RestController
public class RechtController implements IRechtApi {

   @Autowired
   private RechtService _rechtService;

   @Override
   public ResponseEntity<List<Recht>> getAllRecht(final Long rolleid) {
      return new ResponseEntity<>(_rechtService.alleRechtEinerRolle(rolleid), HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Recht> getRechtById(final Long id) {
      return new ResponseEntity<>(_rechtService.getRechtById(id), HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Recht> updateRecht(final Long rechtId, final Recht recht) {
      return new ResponseEntity<>(_rechtService.updateRecht(rechtId, recht), HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Recht> addRecht(final Recht recht) {
      return new ResponseEntity<>(_rechtService.addRecht(recht), HttpStatus.CREATED);
   }


   @Override
   public ResponseEntity<Void> deleteRechtById(final Long rechtid) {
      _rechtService.deleteRechtById(rechtid);
      return new ResponseEntity<>(HttpStatus.OK);
   }
}