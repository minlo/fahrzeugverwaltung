package de.deutscherv.drvit.demo.controller;

import de.deutscherv.drvit.demo.api.IBetriebApi;
import de.deutscherv.drvit.demo.model.Betrieb;
import de.deutscherv.drvit.demo.service.BetriebService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/** Controller für Betrieb.*/
@RestController
public class BetriebController implements IBetriebApi {


   @Autowired
   private BetriebService _betriebService;

   
   @Override
   public ResponseEntity<List<Betrieb>> getAllBetrieb(final Long unternhemenId) {
      return  new  ResponseEntity<>(_betriebService.alleBetriebEinesUnternehmen(unternhemenId), HttpStatus.OK);
   }

   
   @Override
   public ResponseEntity<Betrieb> getBetrieb(final Long id) {
      return new ResponseEntity<>(_betriebService.getBetriebById(id), HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Betrieb> updateBetrieb(final Long betriebId, final Betrieb betrieb) {
      return new ResponseEntity<>(_betriebService.updateBetrieb(betriebId, betrieb), HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Betrieb> addBetrieb(final Betrieb betrieb, final Long unternehmenId) {
      return new ResponseEntity<>(_betriebService.addBetrieb(betrieb, unternehmenId), HttpStatus.CREATED);
   }

   @Override
   public ResponseEntity<Void> deleteBetriebById(final Long betriebId) {
      _betriebService.deleteBetriebById(betriebId);
      return new ResponseEntity<Void>(HttpStatus.OK);
   }

}