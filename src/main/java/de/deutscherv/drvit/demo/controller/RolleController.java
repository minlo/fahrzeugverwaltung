package de.deutscherv.drvit.demo.controller;

import de.deutscherv.drvit.demo.api.IRolleApi;
import de.deutscherv.drvit.demo.model.Rolle;
import de.deutscherv.drvit.demo.service.RolleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/** Controller für Rolle.*/
@RestController
public class RolleController implements IRolleApi {

   @Autowired
   private RolleService _rolleService;

   @Override
   public ResponseEntity<List<Rolle>> getAllRolle() {
      return new ResponseEntity<>(_rolleService.alleRolle(), HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Rolle> getRolleById(final Long id) {
      return new ResponseEntity<>(_rolleService.getRolle(id), HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Rolle> updateRolle(final Long rolleId, final Rolle rolle) {
      return new ResponseEntity<>(_rolleService.updateRolle(rolleId, rolle), HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Rolle> addRolle(final Rolle rolle) {
      return new ResponseEntity<>(_rolleService.addRolle(rolle), HttpStatus.CREATED);
   }


   @Override
   public ResponseEntity<Void> deleteRolleById(final Long rolleId) {
      _rolleService.deleteRolleById(rolleId);
      return new ResponseEntity<>(HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Rolle> addRechtToRole(final Long rechtId, final Long rolleId) {
      return new ResponseEntity<>(_rolleService.addRechtToRole(rechtId, rolleId), HttpStatus.OK);
   }
}