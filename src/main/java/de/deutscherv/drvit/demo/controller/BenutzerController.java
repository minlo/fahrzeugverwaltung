package de.deutscherv.drvit.demo.controller;

import de.deutscherv.drvit.demo.api.IBenutzerApi;
import de.deutscherv.drvit.demo.model.Benutzer;
import de.deutscherv.drvit.demo.service.BenutzerService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/** Controller für Benutzer.*/
@RestController
public class BenutzerController implements IBenutzerApi {

   @Autowired
   private BenutzerService _benutzerService;


   
   @Override
   public ResponseEntity<List<Benutzer>> getAllBenutzer() {
      return  new  ResponseEntity<>(_benutzerService.alleBenutzerSehen(), HttpStatus.OK);
   }

   
   @Override
   public ResponseEntity<Benutzer> getBenutzer(final Long id) {
      return new ResponseEntity<>(_benutzerService.getBenutzerById(id), HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Benutzer> updateBenutzer(final Long benutzerId, final Benutzer benutzer) {
      return new ResponseEntity<>(_benutzerService.updateBenutzer(benutzerId, benutzer), HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Benutzer> addBenutzer(final Benutzer benutzer) {
      return new ResponseEntity<>(_benutzerService.addBenutzer(benutzer), HttpStatus.CREATED);
   }

   @Override
   public ResponseEntity<Void> deleteBenutzer(final Long benutzerId) {
      _benutzerService.deleteBenutzer(benutzerId);
      return new ResponseEntity<>(HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Benutzer> addRolleToBenutzer(final Long roleId, final Long benutzerId) {
      return new ResponseEntity<>(_benutzerService.addRolleToBenutzer(roleId, benutzerId), HttpStatus.OK);
   }
}