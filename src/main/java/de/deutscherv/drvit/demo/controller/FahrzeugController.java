package de.deutscherv.drvit.demo.controller;

import de.deutscherv.drvit.demo.api.IFahrzeugApi;
import de.deutscherv.drvit.demo.model.Fahrzeug;
import de.deutscherv.drvit.demo.service.FahrzeugService;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/** Controller für Fahrzeug.*/
@RestController
public class FahrzeugController implements IFahrzeugApi {

   @Autowired
   private FahrzeugService _fahrzeugService;

   @Override
   public ResponseEntity<List<Fahrzeug>> getAllFahrzeug() {
      return new ResponseEntity<>(_fahrzeugService.alleFahrzeug(), HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Fahrzeug> getFahrzeug(final Long id) {
      return new ResponseEntity<>(_fahrzeugService.getFahrzeugById(id), HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Fahrzeug> updateFahrzeug(final Long id, final Fahrzeug fahrzeug) {
      return new ResponseEntity<>(_fahrzeugService.updateFahrzeugById(id, fahrzeug), HttpStatus.OK );
   }

   @Override
   public ResponseEntity<Fahrzeug> addFahrzeug(final Long betriebid, final Fahrzeug fahrzeug) {
      return new ResponseEntity<>(_fahrzeugService.addFahrzeug(betriebid, fahrzeug), HttpStatus.CREATED);
   }

   @Override
   public ResponseEntity<Void> deleteFahrzeugById(final Long fahrzeugid) {
      _fahrzeugService.deleteFahrzeugById(fahrzeugid);
      return new ResponseEntity<>(HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Boolean> statusFahrzeugVonBis(final Long id, final Date date1, final Date date2) {

      return new ResponseEntity<>(_fahrzeugService.isFahrzeugFreiInZeitraum(id, date1, date2) ,HttpStatus.OK);
   }

   @Override
   public ResponseEntity<List<Fahrzeug>> freiFahrzeugVonBis(final Date date1, final Date date2) {
      return new ResponseEntity<>(_fahrzeugService.getFreieFahrzeuge(date1, date2), HttpStatus.OK);
   }
}