package de.deutscherv.drvit.demo.controller;

import de.deutscherv.drvit.demo.api.IUnternehmenApi;
import de.deutscherv.drvit.demo.model.Unternehmen;
import de.deutscherv.drvit.demo.service.UnternehmenService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/** Controller für Unternehmen.*/
@RestController
public class UnternehmenController implements IUnternehmenApi {

   @Autowired
   private UnternehmenService _service;

   @Override
   public ResponseEntity<List<Unternehmen>> getAllUnternehmen() {
      return new ResponseEntity<>(_service.alleUnternehmen(), HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Unternehmen> getUnternehmen(final Long id) {
      return new ResponseEntity<>(_service.getUnternehmen(id), HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Unternehmen> updateUnternehmen(final Long unternehmenId, final Unternehmen unternehmen) {
      return new ResponseEntity<>(_service.updateUnternehmen(unternehmenId, unternehmen), HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Unternehmen> addUnternehmen(final Long unternehmenId, final Unternehmen unternehmen) {
      return new ResponseEntity<>(_service.addUnternehmen(unternehmen), HttpStatus.CREATED);
   }


   @Override
   public ResponseEntity<Void> deleteUnternehmenById(final Long unternehmenId) {
      _service.deleteUnternehmenById(unternehmenId);
      return new ResponseEntity<>(HttpStatus.OK);
   }
}