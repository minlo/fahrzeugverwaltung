package de.deutscherv.drvit.demo.controller;

import de.deutscherv.drvit.demo.api.IReservierungApi;
import de.deutscherv.drvit.demo.model.Reservierung;
import de.deutscherv.drvit.demo.service.ReservierungService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/** Controller für Reservierung.*/
@RestController
public class ReservierungController implements IReservierungApi {

   @Autowired
   private ReservierungService _reservierungService;

   @Override
   public ResponseEntity<List<Reservierung>> getAllReservierung() {
      return new ResponseEntity<>(_reservierungService.alleReservierung(), HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Reservierung> getReservierung(final Long id) {
      return new ResponseEntity<>(_reservierungService.getReservierung(id), HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Reservierung> updateReservierung(final Long reservierungId, final Reservierung reservierung) {
      return new ResponseEntity<>(_reservierungService.updateReservierung(reservierungId, reservierung), HttpStatus.OK);
   }

   @Override
   public ResponseEntity<Reservierung> addReservierung(final Reservierung reservierung, final Long fahrzeugId, final Long benutzerid) {
      return new ResponseEntity<>(_reservierungService.addReservierung(reservierung, fahrzeugId, benutzerid), HttpStatus.CREATED);
   }


   @Override
   public ResponseEntity<Void> deleteReservierungById(final Long reservierungid) {
      _reservierungService.deleteReservierungById(reservierungid);
      return new ResponseEntity<>(HttpStatus.OK);
   }

}