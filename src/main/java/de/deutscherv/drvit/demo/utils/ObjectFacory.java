package de.deutscherv.drvit.demo.utils;

import de.deutscherv.drvit.demo.dbo.BenutzerDbo;
import de.deutscherv.drvit.demo.dbo.BetriebDbo;
import de.deutscherv.drvit.demo.dbo.FahrzeugDbo;
import de.deutscherv.drvit.demo.dbo.RechtDbo;
import de.deutscherv.drvit.demo.dbo.ReservierungDbo;
import de.deutscherv.drvit.demo.dbo.RolleDbo;
import de.deutscherv.drvit.demo.dbo.UnternehmenDbo;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ObjectFacory {
   
   protected ObjectFacory() {
      
   }
   
   /**
    * Erzeugt ein BenutzerDbo.
    * @return BenutzerDbo
    */
   public static BenutzerDbo getBeneutzerDbo() {
      
      BenutzerDbo benutzerDbo = new BenutzerDbo();
      benutzerDbo.setName("Messi");
      
      return benutzerDbo;
   }
   
   public static UnternehmenDbo getUnternehmenDbo() {
      
      final UnternehmenDbo unternehmenDbo = new UnternehmenDbo();
      unternehmenDbo.setName("VW-GmbH");
      
      return unternehmenDbo;
   }
   
   public static BetriebDbo getBetriebDbo() {
      final BetriebDbo betriebDbo = new BetriebDbo();
      betriebDbo.setName("VW-Betrieb1");
      betriebDbo.setUnternehmen(getUnternehmenDbo());
      
      return betriebDbo;
   }
   
  public static RolleDbo getRolleDbo() {
     final RolleDbo rolleDbo = new RolleDbo();
     rolleDbo.setName("Prüfer");
     return rolleDbo;
  }

   public static RechtDbo getRechtDbo() {
      final RechtDbo rechtDbo = new RechtDbo();
      rechtDbo.setName("ADMIN");
      return rechtDbo;
   }
   
   
   public static FahrzeugDbo getFahrzeugDbo() {
      final FahrzeugDbo fahrzeugDbo = new FahrzeugDbo();
      
      fahrzeugDbo.setModel("C5");
      fahrzeugDbo.setBetrieb(getBetriebDbo());
      return fahrzeugDbo;
   }
   
   public static ReservierungDbo getReserviserungDbo() {
      final ReservierungDbo reservierungDbo = new ReservierungDbo();
      reservierungDbo.setBenutzer(getBeneutzerDbo());
      reservierungDbo.set_fahrzeug(getFahrzeugDbo());
      //reservierungDbo.setVon(new Date(0, 0, 0));
      //reservierungDbo.setBis(new Date(2022, 01, 10));
      return reservierungDbo;
   }
   
   
}