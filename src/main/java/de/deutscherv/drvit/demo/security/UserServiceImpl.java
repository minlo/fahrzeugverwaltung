package de.deutscherv.drvit.demo.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import de.deutscherv.drvit.demo.dbo.BenutzerDbo;
import de.deutscherv.drvit.demo.dbo.RolleDbo;
import de.deutscherv.drvit.demo.service.BenutzerService;

@Service
public class UserServiceImpl implements UserDetailsService {

	@Autowired
	BenutzerService benutzerService;


	private Collection<? extends GrantedAuthority> getAuthorities(Collection<RolleDbo> roles) {
		Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		for (RolleDbo role : roles) {
			grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
			role.getRechts().forEach(rechtDbo -> grantedAuthorities.add(new SimpleGrantedAuthority(rechtDbo.getName())));
		}
		return grantedAuthorities;
	}



	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		System.out.println("######################CHECK USER.......");
		Optional<BenutzerDbo> opt = benutzerService.getBenutzerByName(username);
		
		if(!opt.isPresent()) {
			throw new UsernameNotFoundException("User with email: " +username +" not found");
		} else {
			BenutzerDbo benutzerDbo = opt.get();
			List<RolleDbo> roleEntities = new ArrayList<>(benutzerDbo.getRolle());
			return new  User(opt.get().getName(), null, getAuthorities(roleEntities));
		}
	}

}
