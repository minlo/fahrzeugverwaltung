package de.deutscherv.drvit.demo.api;

import de.deutscherv.drvit.demo.model.Recht;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/** API Interface für Recht.*/
@RequestMapping("recht")
public interface IRechtApi {

   /**
    * Ausgabe aller Recht der Datenbank.
    * @param rolleid fuer Rolle in DB
    * @return Liste Recht-Objekt
    */
   @GetMapping(path = "/all/{id}")
   ResponseEntity<List<Recht>> getAllRecht(@NonNull @PathVariable("id") Long rolleid);

   /**
    * Einen Recht mit der Id von der Datenbank ausgebeb.
    * @param id das zu persistierende Recht-Objekt
    * @return das persistierte Recht-Objekt
    */
   @GetMapping(path = "/{id}")
   ResponseEntity<Recht> getRechtById(@NonNull @PathVariable("id") Long id);

   /**
    * modifiziert einen Recht in der Datenbank.
    * @param recht das zu persistierende Recht-Objekt
    * @param rechtId fuer Recht in DB
    * @return das persistierte Recht-Objekt
    */
   @PutMapping(path = "/{id}")
   ResponseEntity<Recht> updateRecht(@NonNull  @PathVariable("id")  Long rechtId,
      @RequestBody Recht recht);


   /**
    * Fuegt einen Recht der Datenbank hinzu.
    * @param recht das zu persistierende Recht-Objekt
    * @return das persistierte Recht-Objekt
    */
   @PostMapping(path = "/add")
   ResponseEntity<Recht> addRecht(@NonNull @RequestBody Recht recht);

   /**
    * loescht einen Recht von der Datenbank aus .
    * @param rechtid das zu persistierende Recht-Objekt
    * @return das persistierte Recht-Objekt
    */
   @DeleteMapping(path = "/{recht_id}")
   ResponseEntity<Void> deleteRechtById(@NonNull @PathVariable("recht_id") Long rechtid);
}