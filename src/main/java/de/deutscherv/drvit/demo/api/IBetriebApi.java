package de.deutscherv.drvit.demo.api;


import de.deutscherv.drvit.demo.model.Betrieb;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/** API Interface für Betrieb.*/
@RequestMapping("betrieb")
public interface IBetriebApi {

   /**
    * Liefert alle Betriebs von der DB aus.
    * @param unternhemenId Unternehmen - ID in DB
    * @return betriebListe alle Betriebe die zu einem Unternehmen gehoeren
    */
   @GetMapping(path = "/all/{id}")
   ResponseEntity<List<Betrieb>> getAllBetrieb(@NonNull @PathVariable("id") Long unternhemenId);

   /**
    * Einen Betrieb mit der Id von der Datenbank ausgeben.
    * @param id das zu persistierende Betrieb-Objekt
    * @return das persistierte Betrieb-Objekt
    */
   @GetMapping(path = "/{id}")
   ResponseEntity<Betrieb> getBetrieb(@NonNull @PathVariable("id") Long id);


   /**
    * modifiziert einen Betrieb in der Datenbank.
    * @param betrieb das zu persistierende Betrieb-Objekt
    * @param betriebId fuer Betrieb in DB
    * @return das persistierte Betrieb-Objekt
    */
   @PutMapping(path = "/{id}")
   ResponseEntity<Betrieb> updateBetrieb(@NonNull @PathVariable("id") Long betriebId,
      @RequestBody Betrieb betrieb);


   /**
    * Fuegt ein Betrieb der Datenbank hinzu.
    * @param betrieb das zu persistierende Betrieb-Objekt
    * @param unternehmenId fuer Unternehmen in DB
    * @return das persistierte Betrieb-Objekt
    */
   @PostMapping(path = "/add/{unternehmenId}")
   ResponseEntity<Betrieb> addBetrieb(@NonNull @RequestBody Betrieb betrieb,
         @PathVariable("unternehmenId") Long unternehmenId);

   /**
    * loescht ein Betrieb von der  Datenbank aus .
    * @param betriebId das zu persistierende Betrieb-Objekt
    * @return das persistierte Betrieb-Objekt
    */
   @DeleteMapping(path = "/{betrieb_id}")
   ResponseEntity<Void> deleteBetriebById(@NonNull @PathVariable("betrieb_id") Long betriebId);
}