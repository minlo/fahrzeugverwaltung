package de.deutscherv.drvit.demo.api;

import de.deutscherv.drvit.demo.model.Reservierung;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


/** API Interface für Reservierung.*/
@RequestMapping("reservierung")
//@PreAuthorize("hasPermission(principal, 'reservierung')")
public interface IReservierungApi {

   /**
    * Ausgabe aller Reservierung der Datenbank.
    * @return Liste Reservierung-Objekt
    */
   @GetMapping(path = "/all")
   ResponseEntity<List<Reservierung>> getAllReservierung();

   /**
    * Einen Reservierung mit der Id von der Datenbank ausgebeb.
    * @param id das zu persistierende Reservierung-Objekt
    * @return das persistierte Reservierung-Objekt
    */
   @GetMapping(path = "/{id}")
   ResponseEntity<Reservierung> getReservierung(@NonNull @PathVariable("id") Long id);


   /**
    * modifiziert einen Reservierung in der Datenbank.
    * @param reservierung das zu persistierende Reservierung-Objekt
    * @param reservierungId fuer Reservierung
    * @return das persistierte Reservierung-Objekt
    */
   @PutMapping(path = "/{id}")
   ResponseEntity<Reservierung> updateReservierung(@NonNull @PathVariable("id") Long reservierungId,
      @RequestBody Reservierung reservierung);

   /**
    * Fuegt einen Reservierung der Datenbank hinzu.
    * @param reservierung das zu persistierende Reservierung-Objekt
    * @param fahrzeugid fuer Fahrzeug
    * @param benutzerId fuer Benutzer
    * @return das persistierte Reservierung-Objekt
    * @throws Exception wird Eine Exception geworfen falls buntzerid nicht vorhanden
    */
   @PostMapping(path = "/add/fahrzeug/{fahrzeugId}/benutzer/{benutzerId}")
  // @PreAuthorize("hasPermission(principal, 'fahrzeug:mieten')")
   ResponseEntity<Reservierung> addReservierung(@NonNull @RequestBody Reservierung reservierung,
         @PathVariable(name = "fahrzeugId") Long fahrzeugid, @PathVariable(name = "benutzerId") Long benutzerId) throws Exception;


   /**
    * loescht einen Reservierung von der Datenbank aus .
    * @param reservierungid das zu persistierende Reservierung-Objekt
    * @return das persistierte Reservierung-Objekt
    */
   @DeleteMapping(path = "/{reservierungId}")
   ResponseEntity<Void> deleteReservierungById(@NonNull @PathVariable("reservierungId") Long reservierungid);
}