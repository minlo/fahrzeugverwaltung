package de.deutscherv.drvit.demo.api;

import de.deutscherv.drvit.demo.model.Unternehmen;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/** API Interface für Unternehmen.*/
@RequestMapping("unternehmen")
public interface IUnternehmenApi {

   /**
    * Ausgabe aller Unterhemen der Datenbank.
    * @return Liste Unternehmen-Objekt
    */
   @GetMapping(path = "/all")
   ResponseEntity<List<Unternehmen>> getAllUnternehmen();

   /**
    * Ein Unternehmen mit der Id von der Datenbank ausgebeb.
    * @param id das zu persistierende Unternehmen-Objekt
    * @return das persistierte Unternehmen-Objekt
    */
   @GetMapping(path = "{id}")
   ResponseEntity<Unternehmen> getUnternehmen(@NonNull @PathVariable("id") Long  id);

   /**
    * modifiziert einen Unternehmen in der Datenbank.
    * @param unternehmen das zu persistierende Unternehmen-Objekt
    * @Param unternehmenId fuer Unternehmen
    * @param unternehmenId das zu perisstierende Unternehmen-Id
    * @return das persistierte Unternehmen-Objekt
    */
   @PutMapping(path = "/{id}")
   ResponseEntity<Unternehmen> updateUnternehmen(@NonNull @PathVariable("id") Long unternehmenId,
      @RequestBody Unternehmen unternehmen);

   /**
    * Fuegt einen Unternehmen der Datenbank hinzu.
    * @param unternehmen das zu persistierende Unternehmen-Objekt
    * @param unternehmenId fuer Unternehmen
    * @return das persistierte Unternehmen-Objekt
    */
   @PostMapping(path = "/add")
   ResponseEntity<Unternehmen> addUnternehmen(@NonNull Long unternehmenId,
      @RequestBody Unternehmen unternehmen);

   /**
    * loescht einen Unternehmen von der  Datenbank aus .
    * @param unternehmenId das zu persistierende Benutzer-Objekt
    * @return das persistierte Benutzer-Objekt
    */
   @DeleteMapping(path = "/{unternehmen_id}")
   ResponseEntity<Void> deleteUnternehmenById(@NonNull @PathVariable("unternehmen_id") Long unternehmenId);
}