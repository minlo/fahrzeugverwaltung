package de.deutscherv.drvit.demo.api;

import de.deutscherv.drvit.demo.model.Rolle;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/** API Interface für Rolle.*/
@RequestMapping("rolle")
public interface IRolleApi {

   /**
    * Ausgabe aller Rolle der Datenbank.
    * @return Liste Rolle-Objekt
    */
   @GetMapping(path = "/all")
   ResponseEntity<List<Rolle>> getAllRolle();

   /**
    * Einen Rolle mit der Id von der Datenbank ausgebeb.
    * @param id das zu persistierende Rolle-Objekt
    * @return das persistierte Rolle-Objekt
    */
   @GetMapping(path = "/{id}")
   ResponseEntity<Rolle> getRolleById(@NonNull @PathVariable("id") Long id);


   /**
    * modifiziert einen Rolle in der Datenbank.
    * @param rolle das zu persistierende Rolle-Objekt
    * @param rolleId fuer Rolle
    * @return das persistierte Rolle-Objekt
    */
   @PutMapping(path = "/{id}")
   ResponseEntity<Rolle> updateRolle(@NonNull @PathVariable("id") Long rolleId,
      @RequestBody Rolle rolle);


   /**
    * Fuegt einen Rolle der Datenbank hinzu.
    * @param rolle das zu persistierende Rolle-Objekt
    * @return das persistierte Rolle-Objekt
    */
   @PostMapping(path = "/add")
   ResponseEntity<Rolle> addRolle(@NonNull @RequestBody Rolle rolle);


   /**
    * loescht einen Rolle von der Datenbank aus .
    * @param rolleId das zu persistierende Rolle-Objekt
    * @return das persistierte Rolle-Objekt
    */
   @DeleteMapping(path = "/{rolleId}")
   ResponseEntity<Void> deleteRolleById(@NonNull @PathVariable("rolleId") Long rolleId);

   /**
    * fuegt ein Recht zu einer Rolle von der Datenbank aus .
    * @param rolleId das zu persistierende Rolle-Objekt
    * @param rechtId fuer Recht
    * @return das persistierte Rolle-Objekt
    */
   @PostMapping(path = "/recht/{rechtId}/rolle/{rolleId}")
   ResponseEntity<Rolle> addRechtToRole(@PathVariable("rechtId") Long rechtId,
      @PathVariable("rolleId") Long rolleId);
}