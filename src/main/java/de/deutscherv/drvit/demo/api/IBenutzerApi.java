package de.deutscherv.drvit.demo.api;

import de.deutscherv.drvit.demo.model.Benutzer;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/** API Interface für Benutzer.*/
@RequestMapping("benutzer")
public interface IBenutzerApi {

   /**
    * Ausgabe aller Benutzer der Datenbank.
    * @return Liste Benutzer-Objekt
    */
   @GetMapping(path = "/all")
   ResponseEntity<List<Benutzer>> getAllBenutzer();

   /**
    * Einen Benutzer mit der Id von der Datenbank ausgebeb.
    * @param id das zu persistierende Benutzer-Objekt
    * @return das persistierte Benutzer-Objekt
    */
   @GetMapping(path = "/{id}")
   ResponseEntity<Benutzer> getBenutzer(@NonNull @PathVariable("id") Long  id);

   /**
    * modifiziert einen Benutzer in der Datenbank.
    * @param benutzer das zu persistierende Benutzer-Objekt
    * @param benutzerId das zu persistierende Benutzer-ID
    * @return das persistierte Benutzer-Objekt
    */
   @PutMapping(path = "/{id}")
   ResponseEntity<Benutzer> updateBenutzer(@NonNull @PathVariable("id") Long benutzerId,
      @RequestBody Benutzer benutzer);


   /**
    * Fuegt einen Benutzer der Datenbank hinzu.
    * @param benutzer das zu persistierende Benutzer-Objekt
    * @return das persistierte Benutzer-Objekt
    */
   @PostMapping(path = "/add")
   ResponseEntity<Benutzer> addBenutzer(@NonNull @RequestBody Benutzer benutzer);

   /**
    * loescht einen Benutzer von der  Datenbank aus .
    * @param benutzerId das zu persistierende Benutzer-Objekt
    * @return das persistierte Benutzer-Objekt
    */
   @DeleteMapping(path = "/{benutzer_id}")
   ResponseEntity<Void> deleteBenutzer(@NonNull @PathVariable("benutzer_id") Long benutzerId);


   /**
    * fügt einen Rolle zu Benutzer in der Datenbank hin .
    * @param benutzerId das zu persistierende Benutzer-Objekt
    * @param rolleId das zu persistierende Benutzer-Id
    * @return das persistierte Benutzer-Objekt
    */
   @PostMapping(path = "/{benutzer_id}/rolle")
   ResponseEntity<Benutzer> addRolleToBenutzer(@RequestParam(name = "rolleId") Long rolleId,
      @PathVariable("benutzer_id") Long benutzerId);
}