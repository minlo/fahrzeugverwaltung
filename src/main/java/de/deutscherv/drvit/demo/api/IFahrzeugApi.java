package de.deutscherv.drvit.demo.api;

import de.deutscherv.drvit.demo.model.Fahrzeug;
import java.util.Date;
import java.util.List;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/** API Interface für Fahrzeug.*/
@RequestMapping("fahrzeug")
public interface IFahrzeugApi {
   /**
    * Ausgabe aller Fahrzeug der Datenbank.
    * @return Liste Fahrzeug-Objekt
    */
   @GetMapping(path = "/all")
   //@PreAuthorize("hasAnyRole('ADMIN','USER')")
   ResponseEntity<List<Fahrzeug>> getAllFahrzeug();

   /**
    * Einen Fahrzeug mit der Id von der Datenbank ausgebeb.
    * @param id das zu persistierende Fahrzeug-Objekt
    * @return das persistierte Fahrzeug-Objekt
    */
   @GetMapping(path = "/{id}")
   ResponseEntity<Fahrzeug> getFahrzeug(@NonNull @PathVariable("id") Long id);


   /**
    * modifiziert einen Fahrzeug in der Datenbank.
    * @param fahrzeug das zu persistierende Fahrzeug-Objekt
    * @param fahrzeugId fuer Fahrzeug in DB
    * @return das persistierte Fahrzeug-Objekt
    */
   @PutMapping(path = "/{id}")
   ResponseEntity<Fahrzeug> updateFahrzeug(@NonNull @PathVariable("id") Long fahrzeugId,
         @RequestBody Fahrzeug fahrzeug);


   /**
    * Fuegt einen Fahrzeug der Datenbank hinzu.
    * @param betriebId das zu persistierende Betrieb-Objekt
    * @param model fuer Fahrzeug
    * @return das persistierte Fahrzeug-Objekt
    */
   @PostMapping(path = "/add/{betriebId}")
   ResponseEntity<Fahrzeug> addFahrzeug(@PathVariable("betriebId") Long betriebId,
      @RequestBody Fahrzeug model);


   /**
    * loescht einen Fahrzeug von der Datenbank aus .
    * @param fahrzeugid das zu persistierende Fahrzeug-Objekt
    * @return das persistierte Fahrzeug-Objekt
    */
   @DeleteMapping(path = "/{fahrzeug_id}")
   ResponseEntity<Void> deleteFahrzeugById(@NonNull @PathVariable("fahrzeug_id") Long fahrzeugid);

   /**
    * Prueft das Status des Fahrzeug in einem Zeitraum.
    * @param fahrzeugid das zu persistierende Fahrzeug-Objekt
    * @param date1 StartReservierung
    * @param date2 EndReservierung
    * @return true oder false t
    */
   @GetMapping(path = "/{fahrzeugId}/isFrei")
   ResponseEntity<Boolean> statusFahrzeugVonBis(@NonNull @PathVariable("fahrzeugId") Long fahrzeugid,
         @RequestParam("von") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date1,
         @RequestParam("bis") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date2);
   /**
    * liefert eine Liste von freieb Fahrzeuge in einem Zeitraum zurueck.
    * @param date1 StartReservierung
    * @param date2 EndReservierung
    * @return liste von freien Fahzeuge
    */
   @GetMapping(path = "/freiFahrzeugVonBis")
   ResponseEntity<List<Fahrzeug>> freiFahrzeugVonBis(@NonNull @RequestParam("von")
      @DateTimeFormat(pattern = "yyyy-MM-dd") Date date1,
      @RequestParam("bis") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date2);
}