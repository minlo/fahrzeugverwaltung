package de.deutscherv.drvit.demo.assembler;

import de.deutscherv.drvit.demo.dbo.BenutzerDbo;
import de.deutscherv.drvit.demo.model.Benutzer;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Mapping des BenutzerDbo zu Benutzer umd umgekehrt.
 */
@Component
public class BenutzerAssembler {

   @Autowired
   private RolleAssembler _rolleAssembler;

   /**
    * Mapt BenutzerDbo zu Benutzer.
    * @param benutzerDbo BenutzerDbo
    * @return Benutzer
    */
   public Benutzer mapDbo(final BenutzerDbo benutzerDbo) {
      Benutzer benutzer = null;
      if (benutzerDbo != null) {
         benutzer = new Benutzer();
         benutzer.setIdNr(benutzerDbo.getIdNr());
         benutzer.setName(benutzerDbo.getName());
         benutzer.setRolles(benutzerDbo.getRolle().stream()
            .map(rolleDbo -> _rolleAssembler.mapDbo(rolleDbo)).collect(Collectors.toList()));
      }
      return benutzer;
   }

   /**
    * Mapt Benutzer zu BenutzerDbo.
    * @param benutzer Benutzer
    * @return BenutzerDbo
    */
   public BenutzerDbo mapDto(final Benutzer benutzer) {
      BenutzerDbo benutzerDbo = null;
      if (benutzer != null) {
         benutzerDbo = new BenutzerDbo();
         //benutzerDbo.setIdNr(benutzer.getIdNr());
         benutzerDbo.setName(benutzer.getName());
      }
      return benutzerDbo;
   }
}