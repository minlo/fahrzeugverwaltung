package de.deutscherv.drvit.demo.assembler;

import de.deutscherv.drvit.demo.dbo.BetriebDbo;
import de.deutscherv.drvit.demo.model.Betrieb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Mapping des UnternehmenDbo zu Unternehmen umd umgekehrt.
 */
@Component
public class BetriebAssembler {

   @Autowired
   private UnternehmenAssembler _unternehmenAssembler;

   /**
    * Mapt BetriebDbo zu Betrieb.
    * @param betriebDbo BetriebDbo
    * @return betrieb
    */
   public Betrieb mapDbo(final BetriebDbo betriebDbo) {
      Betrieb betrieb = null;

      if (betriebDbo != null) {
         betrieb = new Betrieb();
         betrieb.setIdNr(betriebDbo.getIdNr());
         betrieb.setName(betriebDbo.getName());
         betrieb.setUnternehmen(_unternehmenAssembler.mapDbo(betriebDbo.getUnternehmen()));
      }
      return betrieb;
   }

   /**
    * Mapt Betrieb zu BetriebDbo.
    * @param betrieb Betrieb
    * @return betriebDbo
    */
   public BetriebDbo mapDto(final Betrieb betrieb) {
      BetriebDbo betriebDbo = null;

      if (betrieb != null) {
         betriebDbo = new BetriebDbo();
         //betriebDbo.setIdNr(betrieb.getIdNr());
         betriebDbo.setName(betrieb.getName());
         //betriebDbo.setUnternehmen(_unternehmenAssembler.mapDto(betrieb.getUnternehmen()));
      }
      return betriebDbo;
   }
}