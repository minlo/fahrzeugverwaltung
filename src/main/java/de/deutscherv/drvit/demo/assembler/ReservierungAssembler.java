package de.deutscherv.drvit.demo.assembler;

import de.deutscherv.drvit.demo.dbo.ReservierungDbo;
import de.deutscherv.drvit.demo.model.Reservierung;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Mapping des ReservierungDbo zu Reservierung umd umgekehrt.
 */
@Component
public class ReservierungAssembler {

   @Autowired
   private BenutzerAssembler _benutzerAssembler;

   @Autowired
   private FahrzeugAssembler _fahrzeugAssembler;


   /**
    * Mapt ReservierungDbo zu Reservierung.
    * @param reservierungDbo ReservierungDbo
    * @return reservierung
    */
   public Reservierung mapDbo(final ReservierungDbo reservierungDbo) {
      Reservierung reservierung = null;

      if (reservierungDbo != null) {
         reservierung = new Reservierung();
         reservierung.setIdNr(reservierungDbo.getIdNr());
         reservierung.setStartReservierung(reservierungDbo.getStartReservierung());
         reservierung.setEndReservierung(reservierungDbo.getEndReservierung());
         reservierung.setFahrzeug(_fahrzeugAssembler.mapDbo(reservierungDbo.getFahrzeug()));
         reservierung.setBenutzer(_benutzerAssembler.mapDbo(reservierungDbo.getBenutzer()));
      }

      return reservierung;
   }

   /**
    * Mapt Reservierung zu ReservierungDbo.
    * @param reservierung Reservierung
    * @return reservierungDbo
    */
   public ReservierungDbo mapDto(final Reservierung reservierung) {
      ReservierungDbo reservierungDbo = null;

      if (reservierung != null) {
         reservierungDbo = new ReservierungDbo();
         reservierungDbo.setStartReservierung(reservierung.getStartReservierung());
         reservierungDbo.setEndReservierung(reservierung.getEndReservierung());
      }
      return reservierungDbo;
   }
}