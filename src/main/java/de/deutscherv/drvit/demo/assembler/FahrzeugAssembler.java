package de.deutscherv.drvit.demo.assembler;

import de.deutscherv.drvit.demo.dbo.FahrzeugDbo;
import de.deutscherv.drvit.demo.model.Fahrzeug;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Mapping des BetriebDbo zu Betrieb umd umgekehrt.
 */
@Component
public class FahrzeugAssembler {

   @Autowired
   private BetriebAssembler _betriebAssembler;


   /**
    * Mapt FahrzeugDbo zu Fahrzeug.
    * @param fahrzeugDbo FahrzeugDbo
    * @return fahrzeug
    */
   public Fahrzeug mapDbo(final FahrzeugDbo fahrzeugDbo) {
      Fahrzeug fahrzeug = null;

      if (fahrzeugDbo != null) {
         fahrzeug = new Fahrzeug();
         fahrzeug.setIdNr(fahrzeugDbo.getIdNr());
         fahrzeug.setModel(fahrzeugDbo.getModel());
         fahrzeug.setBetrieb(_betriebAssembler.mapDbo(fahrzeugDbo.getBetrieb()));
      }
      return fahrzeug;
   }


   /**
    * Mapt Fahrzeug zu FahrzeugDbo.
    * @param fahrzeug Fahrzeug
    * @return fahrzeugDbo
    */
   public FahrzeugDbo mapDto(final Fahrzeug fahrzeug) {
      FahrzeugDbo fahrzeugDbo = null;

      if (fahrzeug != null) {
         fahrzeugDbo = new FahrzeugDbo();
         //fahrzeugDbo.setIdNr(fahrzeug.getIdNr());
         fahrzeugDbo.setModel(fahrzeug.getModel());
         //fahrzeugDbo.setBetrieb(_betriebAssembler.mapDto(fahrzeug.getBetrieb()));
      }
      return fahrzeugDbo;
   }
}