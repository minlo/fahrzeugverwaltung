package de.deutscherv.drvit.demo.assembler;

import de.deutscherv.drvit.demo.dbo.RolleDbo;
import de.deutscherv.drvit.demo.model.Rolle;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
/**
 * Mapping des RolleDbo zu Rolle umd umgekehrt.
 */
@Component
public class RolleAssembler {

   @Autowired
   private RechtAssembler _rechtAssembler;

   /**
    * Mapt Rolle zu RolleDbo.
    * @param rolle Rolle
    * @return rolleDbo
    */
   public RolleDbo mapDto(final Rolle rolle) {
      RolleDbo rolleDbo = null;

      if (rolle != null) {
         rolleDbo = new RolleDbo();
         //rolleDbo.setIdNr(rolle.getIdNr());
         rolleDbo.setName(rolle.getName());
      }
      return rolleDbo;
   }

   /**
    * Mapt RolleDbo zu Rolle.
    * @param rolleDbo RolleDbo
    * @return rolle
    */
   public Rolle mapDbo(final RolleDbo rolleDbo) {
      Rolle rolle = null;

      if (rolleDbo != null) {
         rolle =  new Rolle();
         rolle.setIdNr(rolleDbo.getIdNr());
         rolle.setName(rolleDbo.getName());
         rolle.setRechts(rolleDbo.getRechts().stream().
               map(rechtDbo -> _rechtAssembler.mapDbo(rechtDbo)).collect(Collectors.toList()));
      }
      return rolle;
   }
}
