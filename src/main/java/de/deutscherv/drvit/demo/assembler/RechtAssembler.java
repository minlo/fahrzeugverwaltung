package de.deutscherv.drvit.demo.assembler;

import de.deutscherv.drvit.demo.dbo.RechtDbo;
import de.deutscherv.drvit.demo.model.Recht;
import org.springframework.stereotype.Component;

/**
 * Mapping des RechtDbo zu Recht umd umgekehrt.
 */
@Component
public class RechtAssembler {

   /**
    * Mapt RechtDbo zu Recht.
    * @param rechtDbo RechtDbo
    * @return recht
    */

   public Recht mapDbo(final RechtDbo rechtDbo) {
      Recht recht = null;

      if (rechtDbo != null) {
         recht = new Recht();
         recht.setIdNr(rechtDbo.getIdNr());
         recht.setName(rechtDbo.getName());
      }
      return recht;
   }

   /**
    * Mapt Recht zu RechtDbo.
    * @param recht Recht
    * @return fahrzeug
    */
   public RechtDbo mapDto(final Recht recht) {
      RechtDbo rechtDbo = null;
      if (recht != null) {
         rechtDbo = new RechtDbo();
         rechtDbo.setName(recht.getName());
      }
      return rechtDbo;
   }
}