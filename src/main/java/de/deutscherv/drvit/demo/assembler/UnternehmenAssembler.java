package de.deutscherv.drvit.demo.assembler;

import de.deutscherv.drvit.demo.dbo.UnternehmenDbo;
import de.deutscherv.drvit.demo.model.Unternehmen;
import org.springframework.stereotype.Component;

/**
 * Mapping des UnternehmenDbo zu Unternehmen umd umgekehrt.
 */
@Component
public class UnternehmenAssembler {

   /**
    * Mapt UnternehmenDbo zu Unternehmen.
    * @param unternehmenDbo UnternehmenDbo
    * @return unternehmen
    */
   public Unternehmen mapDbo(final UnternehmenDbo unternehmenDbo) {
      Unternehmen unternehmen = null;

      if (unternehmenDbo != null) {
         unternehmen = new Unternehmen();
         unternehmen.setIdNr(unternehmenDbo.getIdNr());
         unternehmen.setName(unternehmenDbo.getName());
      }
      return unternehmen;
   }

   /**
    * Mapt Unternehmen zu UnternehmenDbo.
    * @param unternehmen Unternehmen
    * @return unternehmenDbo
    */
   public UnternehmenDbo mapDto(final Unternehmen unternehmen) {
      UnternehmenDbo unternehmenDbo = null;

      if (unternehmen != null) {
         unternehmenDbo = new UnternehmenDbo();
         //unternehmenDbo.setIdNr(unternehmen.getIdNr());
         unternehmenDbo.setName(unternehmen.getName());
      }
      return unternehmenDbo;
   }
}
