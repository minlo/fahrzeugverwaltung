package de.deutscherv.drvit.demo.dbo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Datenbankobjekt für FAHRZEUG.
 */
@Entity
@Table(name = "FAHRZEUG")
public class FahrzeugDbo {

   /** ID. */
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "ID")
   private Long _idNr;

   /** Model vom Fahrzeug */
   @Column(name = "MODEL")
   private String _model;

   @ManyToOne(fetch = FetchType.EAGER)
   @JoinColumn(name = "BETRIEB_ID")
   private BetriebDbo _betrieb;

   /**
    * Getter _idNr.
    * @return _idNr
    */
   public Long getIdNr() {
      return _idNr;
   }

   /**
    * Setter idNr.
    * @param idNr neue idNr
    */
   public void setIdNr(final Long idNr) {
      this._idNr = idNr;
   }

   /**
    * Getter _model.
    * @return _model
    */
   public String getModel() {
      return _model;
   }

   /**
    * Setter model.
    * @param model neue model
    */
   public void setModel(final String model) {
      this._model = model;
   }

   /**
    * Getter {@link #_betrieb}.
    * @return {@link #_betrieb}
    */
   public BetriebDbo getBetrieb() {
      return _betrieb;
   }

   /**
    * Setter betriebid.
    * @param betriebid neue betriebid
    */
   public void setBetrieb(final BetriebDbo betriebid) {
      this._betrieb = betriebid;
   }
}