package de.deutscherv.drvit.demo.dbo;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * Datenbankobjekt für Betrieb.
 */
@Entity
@Table(name = "BETRIEB")
public class BetriebDbo {
   
   /** ID. */
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "ID")
   private Long _idNr;
   
   /** name des Betriebs */
   @Column(name = "NAME")
   private String _name;
   
   /** name des Betriebs */
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "UNTERNEHMEN_ID", updatable = true)
   private UnternehmenDbo _unternehmen;
   
   @OneToMany(mappedBy = "_idNr")
   private List<FahrzeugDbo> _fahrzeug = new ArrayList<>();
   
   /**
    * Getter {@link #_idNr}.
    * @return {@link #_idNr}
    */
   public Long getIdNr() {
      return _idNr;
   }
   
   /**
    * Setter {@link #idNr}.
    * @param idNr neue {@link #idNr}
    */
   public void setIdNr(final Long idNr) {
      this._idNr = idNr;
   }
   
   /**
    * Getter {@link #_name}.
    * @return {@link #_name}
    */
   public String getName() {
      return _name;
   }
   
   /**
    * Setter {@link #name}.
    * @param name neue {@link #name}
    */
   public void setName(final String name) {
      this._name = name;
   }
   
   /**
    * Getter {@link #_unternehmen}.
    * @return {@link #_unternehmen}
    */
   public UnternehmenDbo getUnternehmen() {
      return _unternehmen;
   }
   
   /**
    * Setter unternehmen.
    * @param unternehmen Unternehmen
    */
   public void setUnternehmen(final UnternehmenDbo unternehmen) {
      this._unternehmen = unternehmen;
   }
   
}