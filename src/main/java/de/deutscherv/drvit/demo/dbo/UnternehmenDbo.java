package de.deutscherv.drvit.demo.dbo;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Datenbankobjekt für UNTERNEHMEN.
 */
@Entity
@Table(name = "UNTERNEHMEN")
public class UnternehmenDbo {

   /** ID. */
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "ID")
   private Long _idNr;

   /** Name des Unternehmens */
   @Column(name = "NAME")
   private String _name;

   @OneToMany(mappedBy = "_idNr", cascade = CascadeType.REMOVE)
   private List<BetriebDbo> _betrieb = new ArrayList<>();

   /**
    * Getter _idNr.
    * @return _idNr
    */
   public Long getIdNr() {
      return _idNr;
   }

   /**
    * Setter idNr.
    * @param idNr neue idNr
    */
   public void setIdNr(final Long idNr) {
      this._idNr = idNr;
   }

   /**
    * Getter _name.
    * @return _name
    */
   public String getName() {
      return _name;
   }

   /**
    * Setter name.
    * @param name neue name
    */
   public void setName(final String name) {
      this._name = name;
   }
   
   /**
    * Getter _name.
    * @return _name
    */
   public List<BetriebDbo> getBetrieb() {
	return _betrieb;
   }
   
   /**
    * Setter name.
    * @param name neue name
    */
   public void setBetrieb(List<BetriebDbo> betrieb) {
	this._betrieb = betrieb;
   }
   
   
}