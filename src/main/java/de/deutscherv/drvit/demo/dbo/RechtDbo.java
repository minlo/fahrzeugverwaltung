package de.deutscherv.drvit.demo.dbo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Datenbankobjekt für RECHT.
 */
@Entity
@Table(name = "RECHT")
public class RechtDbo {


   /** ID. */
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "ID")
   private Long _idNr;

   /** Rechtsbeschreibung */
   @Column(name = "NAME")
   private String _name;
   
   @ManyToMany(mappedBy = "_rechts")
   private List<RolleDbo> _rolle = new ArrayList<>();

//   @ManyToOne(fetch = FetchType.LAZY, optional = true)
//   @JoinColumn(name = "ROLLE_ID")
//   private RolleDbo _rolle;

   /**
    * Getter _idNr.
    * @return _idNr
    */
   public Long getIdNr() {
      return _idNr;
   }

   /**
    * Setter idNr.
    * @param idNr neue idNr
    */
   public void setIdNr(final Long idNr) {
      this._idNr = idNr;
   }

   /**
    * Getter _name.
    * @return _name
    */
   public String getName() {
      return _name;
   }

   /**
    * Setter name.
    * @param name neue name
    */
   public void setName(final String name) {
      this._name = name;
   }

   /**
    * Getter _rolle.
    * @return _rolle
    */
   public List<RolleDbo> getRolle() {
	return _rolle;
   }

   /**
    * Setter rolle.
    * @param rolle neue rolle
    */
   public void setRolle(List<RolleDbo> rolle) {
	this._rolle = rolle;
   }

   /**
    * Getter _rolle.
    * @return _rolle
    */
//   public RolleDbo getRolle() {
//      return _rolle;
//   }

   /**
    * Setter rolle.
    * @param rolle neue rolle
    */
//   public void setRolle(final RolleDbo rolle) {
//      this._rolle = rolle;
//   }
  
}