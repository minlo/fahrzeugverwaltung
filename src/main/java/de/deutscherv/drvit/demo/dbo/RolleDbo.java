package de.deutscherv.drvit.demo.dbo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * Datenbankobjekt für Rolle.
 */
@Entity
@Table(name = "ROLLE")
public class RolleDbo {

   /** ID. */
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "ID")
   private Long _idNr;

   /** Rolle des Rolles */
   @Column(name = "ROLLE_NAME")
   private String _name;

   @ManyToMany(mappedBy = "_rolle")
   private Collection<BenutzerDbo> benutzerDbos;
   
   @ManyToMany
   @JoinTable(name = "rolle_privileges", 
   joinColumns = @JoinColumn(name = "rolle_id", referencedColumnName = "ID"),
   inverseJoinColumns = @JoinColumn(name = "recht_id", referencedColumnName = "ID"))
   private List<RechtDbo> _rechts = new ArrayList<>();

//   @OneToMany(mappedBy = "_idNr", cascade = CascadeType.ALL, targetEntity = RechtDbo.class)
//   private List<RechtDbo> _rechts = new ArrayList<>();

   /**
    * Getter _idNr.
    * @return _idNr
    */
   public Long getIdNr() {
      return _idNr;
   }

   /**
    * Setter idNr.
    * @param idNr neue idNr
    */
   public void setIdNr(final Long idNr) {
      this._idNr = idNr;
   }

   /**
    * Getter _name.
    * @return _name
    */
   public String getName() {
      return _name;
   }

   /**
    * Setter name.
    * @param name neue name
    */
   public void setName(final String name) {
      this._name = name;
   }

   /**
    * Getter _rechts.
    * @return _rechts
    */
   public List<RechtDbo> getRechts() {
      return _rechts;
   }

   /**
    * Setter rechts.
    * @param rechts neue rechts
    */
   public void setRechts(final List<RechtDbo> rechts) {
      this._rechts = rechts;
   }
}