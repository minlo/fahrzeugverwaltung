package de.deutscherv.drvit.demo.dbo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.ManyToMany;

/**
 * Datenbankobjekt für BENUTZER.
 */
@Entity
@Table(name = "BENUTZER")
public class BenutzerDbo {

   /** ID. */
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "ID")
   private Long _idNr;

   /** Name des Benutzers */
   @Column(name = "BENUTZER_NAME")
   private String _name;

   /** Liste von RolleDbo */
   @ManyToMany
   @JoinTable(name = "benutzer_role", 
   joinColumns = @JoinColumn(name = "benutzer_id", referencedColumnName = "ID"), 
   inverseJoinColumns = @JoinColumn(name = "rolle_id", referencedColumnName = "ID"))
   private List<RolleDbo> _rolle = new ArrayList<>();

   /** Liste ReservierungDbo  */
   @OneToMany(mappedBy = "_idNr")
   private List<ReservierungDbo> _reservierung = new ArrayList<>();

   /**
    * Getter {@link #_idNr}.
    * @return {@link #_idNr}
    */
   public Long getIdNr() {
      return _idNr;
   }

   /**
    * Setter {@link #idNr}.
    * @param idNr neue {@link #idNr}
    */
   public void setIdNr(final Long idNr) {
      this._idNr = idNr;
   }

   /**
    * Getter {@link #_name}.
    * @return {@link #_name}
    */
   public String getName() {
      return _name;
   }

   /**
    * Setter {@link #name}.
    * @param name neue {@link #name}
    */
   public void setName(final String name) {
      this._name = name;
   }

   /**
    * Getter {@link #_rolle}.
    * @return {@link #_rolle}
    */
   public List<RolleDbo> getRolle() {
      return _rolle;
   }

   /**
    * Setter {@link #_rolle}.
    * @param rolle neue {@link #_rolle}
    */
   public void setRolle(final List<RolleDbo> rolle) {
      this._rolle = rolle;
   }

   /**
    * Getter {@link #_idNr}.
    * @return {@link #_idNr}
    */
   public List<ReservierungDbo> getReservierung() {
      return _reservierung;
   }

   /**
    * Setter {@link #_reservierung}.
    * @param reservierung neue {@link #_reservierung}
    */
   public void setReservierung(final List<ReservierungDbo> reservierung) {
      this._reservierung = reservierung;
   }
}