package de.deutscherv.drvit.demo.dbo;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Datenbankobjekt für RESERVIERUNG.
 */
@Entity
@Table(name = "RESERVIERUNG")
public class ReservierungDbo {

   /** ID. */
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "ID")
   private Long _idNr;

   @Column(name = "START_RESERVIERUNG")
   @DateTimeFormat(pattern = "yyyy-MM-dd")
   private Date _startReservierung;

   @Column(name = "END_ENDRESERVIERUNG")
   @DateTimeFormat(pattern = "yyyy-MM-dd")
   private Date _endReservierung;

   @ManyToOne(fetch = FetchType.EAGER)
   @JoinColumn(name = "BENUTZER_ID")
   private BenutzerDbo _benutzer;

   @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
   @JoinColumn(name = "FahrzeugId")
   private FahrzeugDbo _fahrzeug;

   /**
    * Getter _idNr.
    * @return _idNr
    */
   public Long getIdNr() {
      return _idNr;
   }

   /**
    * Setter idNr.
    * @param idNr neue idNr
    */
   public void setIdNr(final Long idNr) {
      this._idNr = idNr;
   }

   /**
    * Getter _idNr.
    * @return _idNr
    */
   public BenutzerDbo getBenutzer() {
      return _benutzer;
   }


   /**
    * Setter benutzer.
    * @param benutzer neue benutzer
    */
   public void setBenutzer(final BenutzerDbo benutzer) {
      this._benutzer = benutzer;
   }


   /**
    * Getter _startReservierung.
    * @return _startReservierung
    */
   public Date getStartReservierung() {
      return _startReservierung;
   }

   /**
    * Setter date.
    * @param date neue date
    */
   public void setStartReservierung(final Date date) {
      _startReservierung = date;
   }

   /**
    * Getter _endReservierung.
    * @return _endReservierung
    */
   public Date getEndReservierung() {
      return _endReservierung;
   }

   /**
    * Setter endReservierung.
    * @param endReservierung neue endReservierung
    */
   public void setEndReservierung(final Date endReservierung) {
      _endReservierung = endReservierung;
   }

   /**
    * Getter _idNr.
    * @return _idNr
    */
   public FahrzeugDbo getFahrzeug() {
      return _fahrzeug;
   }

   /**
    * Setter fahrzeug.
    * @param fahrzeug neue fahrzeug
    */
   public void setFahrzeug(final FahrzeugDbo fahrzeug) {
      this._fahrzeug = fahrzeug;
   }
}