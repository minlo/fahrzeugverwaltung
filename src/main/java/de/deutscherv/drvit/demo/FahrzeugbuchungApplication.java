package de.deutscherv.drvit.demo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@Configuration
@EnableJpaRepositories
@EntityScan(basePackages = {"de.deutscherv.drvit.demo.dbo"})
public class FahrzeugbuchungApplication {

    public static void main(String[] args) {
        SpringApplication.run(FahrzeugbuchungApplication.class, args);
    }

}