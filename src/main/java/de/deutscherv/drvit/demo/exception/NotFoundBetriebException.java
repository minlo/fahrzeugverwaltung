package de.deutscherv.drvit.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundBetriebException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2040854456780152709L;

	public NotFoundBetriebException(String message) {
		super(message);
	}
	
	

}
