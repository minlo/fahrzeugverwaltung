package de.deutscherv.drvit.demo.exception;

public class ExistRolleException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4396665586918087148L;

	public ExistRolleException(String message) {
		super(message);
	}
	
	

}
