package de.deutscherv.drvit.demo.exception;

public class ExistUnternehmenException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 551898176278827238L;

	
	//Constructor
	public ExistUnternehmenException(String message) {
		super(message);
	}

	
	

}
