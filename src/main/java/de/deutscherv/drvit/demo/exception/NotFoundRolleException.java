package de.deutscherv.drvit.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundRolleException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7169909946473211680L;

	public NotFoundRolleException(String message) {
		super(message);
	}
	
	

}
