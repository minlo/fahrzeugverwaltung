package de.deutscherv.drvit.demo.exception;

public class ExistBetriebException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8908450234712810697L;

	public ExistBetriebException(String message) {
		super(message);
		
	}
	
	

}
