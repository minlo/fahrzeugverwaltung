package de.deutscherv.drvit.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundRechtException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3416298382485754998L;

	public NotFoundRechtException(String message) {
		super(message);
	}
	
	

}
