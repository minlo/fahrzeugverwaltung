package de.deutscherv.drvit.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundFahrzeugException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6362966605098685174L;

	public NotFoundFahrzeugException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	
	

}
