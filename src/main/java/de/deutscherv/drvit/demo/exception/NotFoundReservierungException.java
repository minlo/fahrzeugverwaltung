package de.deutscherv.drvit.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundReservierungException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7316645671544848784L;

	public NotFoundReservierungException(String message) {
		super(message);
	}
	
	

}
