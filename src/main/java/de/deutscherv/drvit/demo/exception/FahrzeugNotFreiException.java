package de.deutscherv.drvit.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS)
public class FahrzeugNotFreiException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6362966605098685174L;

	public FahrzeugNotFreiException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	
	

}
