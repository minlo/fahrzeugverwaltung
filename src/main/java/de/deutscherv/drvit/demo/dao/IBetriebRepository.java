package de.deutscherv.drvit.demo.dao;

import de.deutscherv.drvit.demo.dbo.BetriebDbo;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Interface IBetrieb.
 */
@Repository
public interface IBetriebRepository extends JpaRepository<BetriebDbo, Long> {

   /**
    * Lädt BetriebDbo zu einem UnternehmenDbo.
    * @param unternehmenId UnternehmenDbo.
    * @return unternehmenId UntermehmenDbo
    */
   @Query("select b from BetriebDbo b where b._unternehmen._idNr = :unternehmenId")
   List<BetriebDbo> findBy_UnternehmenId(@Param("unternehmenId")  Long unternehmenId);

}