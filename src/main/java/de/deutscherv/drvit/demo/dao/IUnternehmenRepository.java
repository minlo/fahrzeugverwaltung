package de.deutscherv.drvit.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.deutscherv.drvit.demo.dbo.UnternehmenDbo;

/**
 * Interface IUnternehmen.
 */
@Repository
public interface IUnternehmenRepository extends JpaRepository<UnternehmenDbo, Long> {

	

}
