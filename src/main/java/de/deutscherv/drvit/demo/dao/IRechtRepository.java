package de.deutscherv.drvit.demo.dao;

import de.deutscherv.drvit.demo.dbo.RechtDbo;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Interface IRecht.
 */
@Repository
public interface IRechtRepository extends JpaRepository<RechtDbo, Long> {

     /**
       * Lädt einen Recht über den Namen.
       * @param name Name
       * @return Optional RechtDbo
       */
   @Query("select b from RechtDbo b where b._name = :name")
      Optional<RechtDbo> findRechtByName(@Param("name") String name);

      /**
       * Lädt einen Rolle über den Namen.
       * @param rolleid RolleDbo
       * @return Optional RolleDbo
       */
   @Query("select b from RechtDbo b where b._rolle = : rolleId")
     List<RechtDbo> findBy_Rolleid(@Param("rolleid") Long rolleid);


}