package de.deutscherv.drvit.demo.dao;

import de.deutscherv.drvit.demo.dbo.ReservierungDbo;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Interface IReservierung.
 */
@Repository
public interface IReservierungRepository extends JpaRepository<ReservierungDbo, Long> {

   /**
    * Lädt einen Recht über den Namen.
    * @param fahrzeugId ReservierungDbo
    * @param startReservierung Date
    * @param endReservierung Date
    * @return Liste von ReservierungDbo
    */
   @Query(value = "select rb from ReservierungDbo rb where rb._fahrzeug._idNr = :fahrzeugId "
      + "and rb._startReservierung >= :startReservierung and rb._endReservierung <= :endReservierung")
    List<ReservierungDbo> findByFahrzeugAndStartReservierungAfterAndEndReservierungBefore(@Param("fahrzeugId")  Long fahrzeugId,
       @Param("startReservierung") Date startReservierung,  @Param("endReservierung") Date endReservierung);

}