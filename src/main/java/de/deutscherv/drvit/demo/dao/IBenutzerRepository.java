package de.deutscherv.drvit.demo.dao;

import de.deutscherv.drvit.demo.dbo.BenutzerDbo;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface IBenutzer.
 */
@Repository
public interface IBenutzerRepository extends JpaRepository<BenutzerDbo, Long> {

   /**
    * Lädt alle Arbeitgebervertreter oder Arbeitnehmervertreter zu einer Vertretungsart und Mandanten.
    * @param username BenutzerDbo.
    * @return benutzerDbo BenutzerDbo
    */
   Optional<BenutzerDbo> findBy_name(final String username);

}