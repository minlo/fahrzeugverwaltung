package de.deutscherv.drvit.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.deutscherv.drvit.demo.dbo.FahrzeugDbo;

/**
 * Interface IFahrzeug.
 */
@Repository
public interface IFahrzeugRepository extends JpaRepository<FahrzeugDbo, Long> {
	
	

}
