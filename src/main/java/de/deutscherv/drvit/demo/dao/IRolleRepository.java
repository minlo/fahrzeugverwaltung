package de.deutscherv.drvit.demo.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.deutscherv.drvit.demo.dbo.RolleDbo;
import de.deutscherv.drvit.demo.model.Rolle;

/**
 * Interface IRolle.
 */
@Repository 
public interface IRolleRepository extends JpaRepository<RolleDbo, Long> {

	//Optional<Rolle> findRolleByName(Rolle rolle);

}
