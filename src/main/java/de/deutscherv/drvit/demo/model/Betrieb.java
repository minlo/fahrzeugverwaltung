package de.deutscherv.drvit.demo.model;

public class Betrieb {
	
	
	private Long _idNr;
	private String _name;
	private Unternehmen _unternehmen;
	
	public Long getIdNr() {
		return _idNr;
	}
	
	public void setIdNr(Long idNr) {
		this._idNr = idNr;
	}
	
	public String getName() {
		return _name;
	}
	
	public void setName(String name) {
		this._name = name;
	}
	
	public Unternehmen getUnternehmen() {
		return _unternehmen;
	}
	
	public void setUnternehmen(Unternehmen unternehmen) {
		this._unternehmen = unternehmen;
	}
	
	

}
