package de.deutscherv.drvit.demo.model;

import java.util.List;

public class Benutzer {
	
	
	private Long _idNr;
	private String _name;
	private List<Rolle> rolles;
	
	public Long getIdNr() {
		return _idNr;
	}
	
	public void setIdNr(Long idNr) {
		this._idNr = idNr;
	}
	
	public String getName() {
		return _name;
	}
	
	public void setName(String name) {
		this._name = name;
	}

	public List<Rolle> getRolles() {
		return rolles;
	}

	public void setRolles(List<Rolle> rolles) {
		this.rolles = rolles;
	}
	
	
}
