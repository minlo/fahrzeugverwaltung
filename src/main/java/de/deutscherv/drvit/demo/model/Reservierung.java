package de.deutscherv.drvit.demo.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Reservierung {
   
   private Long _idNr;
   
   @DateTimeFormat(pattern="yyyy-MM-dd")
   private Date _startReservierung;
   
   @DateTimeFormat(pattern="yyyy-MM-dd")
   private Date _endReservierung;
   
   private Benutzer _benutzer;
   private Fahrzeug _fahrzeug;
   
   public Fahrzeug getFahrzeug() {
      return _fahrzeug;
   }
   public void setFahrzeug(Fahrzeug fahrzeug) {
      this._fahrzeug = fahrzeug;
   }
   public Long getIdNr() {
      return _idNr;
   }
   public void setIdNr(Long idNr) {
      this._idNr = idNr;
   }
   
   public Benutzer getBenutzer() {
      return _benutzer;
   }
   public void setBenutzer(Benutzer _benutzer) {
      this._benutzer = _benutzer;
   }
   public Date getStartReservierung() {
      return _startReservierung;
   }
   public void setStartReservierung(Date startReservierung) {
      _startReservierung = startReservierung;
   }
   public Date getEndReservierung() {
      return _endReservierung;
   }
   public void setEndReservierung(Date endReservierung) {
      _endReservierung = endReservierung;
   }
}