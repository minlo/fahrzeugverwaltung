package de.deutscherv.drvit.demo.model;

public class Fahrzeug {
	   
	   
	   private Long _idNr;
	   private String _model;
	   private Betrieb _betriebid;
	   
	   public Long getIdNr() {
	      return _idNr;
	   }
	   public void setIdNr(Long idNr) {
	      this._idNr = idNr;
	   }
	   public String getModel() {
	      return _model;
	   }
	   public void setModel(String model) {
	      this._model = model;
	   }
	   public Betrieb getBetrieb() {
	      return _betriebid;
	   }
	   public void setBetrieb(Betrieb betriebid) {
	      this._betriebid = betriebid;
	   }
	   
	}