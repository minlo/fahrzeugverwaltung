package de.deutscherv.drvit.demo.model;

import java.util.List;

public class Rolle {
	
	
	private Long _idNr;
	private String _name;
	private List<Recht> rechts;
	 
	
	public Long getIdNr() {
		return _idNr;
	}
	
	public void setIdNr(Long idNr) {
		this._idNr = idNr;
	}
	
	public String getName() {
		return _name;
	}
	
	public void setName(String name) {
		this._name = name;
	}

	public List<Recht> getRechts() {
		return rechts;
	}

	public void setRechts(List<Recht> rechts) {
		this.rechts = rechts;
	}
	
	
	
}
