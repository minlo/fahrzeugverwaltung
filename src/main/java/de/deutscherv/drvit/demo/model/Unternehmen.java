package de.deutscherv.drvit.demo.model;

public class Unternehmen {
	
	private Long _idNr;
	private String _name;
	
	public Long getIdNr() {
		return _idNr;
	}
	public void setIdNr(Long idNr) {
		this._idNr = idNr;
	}
	public String getName() {
		return _name;
	}
	public void setName(String name) {
		this._name = name;
	}
	
}
